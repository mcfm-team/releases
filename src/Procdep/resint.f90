!
!  SPDX-License-Identifier: GPL-3.0-or-later
!  Copyright (C) 2019-2022, respective authors of MCFM.
!
     
    module ResummationIntegration
        use types
        use qtResummation_params
        implicit none

        public :: resint
        public :: resexpint

        private

        logical, parameter :: boostBorn = .true.
        ! do not set to .true.!
        logical, parameter :: phaseUseQt = .false.

        contains

        function resint(r,wgt)
            use ieee_arithmetic
            use types
            use Scalevar
            use PDFerrors
            use SCET
            use MCFMStorage
            use qtResummation
            use LHAPDF
            use MCFMSetupPlots
            use SafetyCuts, only : passed_smallnew
            implicit none
            include 'constants.f'
            include 'mxpart.f'
            include 'vegas_common.f'
            include 'kprocess.f'
            include 'kpart.f'
            include 'energy.f'
            include 'npart.f'
            include 'dynamicscale.f'
            include 'initialscales.f'
            include 'scalevar.f'
            include 'scale.f'
            include 'facscale.f'
            include 'qcdcouple.f'
            include 'couple.f'
            include 'nlooprun.f'
            include 'x1x2.f'
            include 'xmin.f'
            include 'nf.f'
            include 'nflav.f'
            include 'nwz.f'
            include 'nproc.f'
            include 'ipsgen.f'
            include 'masses.f'
            include 'beamtype.f'
            logical:: bin
            common/bin/bin
            real(dp) :: BrnRat
            common/BrnRat/BrnRat

            real(dp) :: resint
            real(dp), intent(in) :: r(mxdim), wgt

            real(dp):: p(mxpart,4),pjet(mxpart,4),xmsq,xmsqMu,xmsqPdf
            real(dp) :: val,pswt,flux

            real(dp) :: p_born(mxpart,4)

            ! functions
            logical :: includedipole

            real(dp) :: xjac
            real(dp) :: qt, phi
            real(dp) :: phaseqt
            real(dp) :: safety

            integer :: j

            resint = 0._dp

            currentPDF = 0
            currentNd = 0

            if (doScalevar .and. bin) then
                scalereweight(:) = 1._dp
            endif

            p(:,:) = 0._dp
            pjet(:,:) = 0._dp

            if (qtmaxRes .eq. qtminRes) then
                qt = qtminRes
                xjac = 2._dp*qt
            else
                qt = r(ndim)*(qtmaxRes-qtminRes) + qtminRes
                xjac = 2._dp*qt*(qtmaxRes-qtminRes)
            endif
            
            phi = 2._dp*pi*r(ndim-1)

    !       ! for debugging purposes we can fix qt here
            !qt = 8.0_dp
            !xjac = 2._dp*qt

            if (qt > qtmaxRes) then
                resint = 0._dp
                return
            endif

            if (qt < qtminRes) then
                resint = 0._dp
                return
            endif

            p(:,:) = 0._dp

            if (phaseUseQt) then
                phaseqt = qt
            else
                phaseqt = 0._dp
            endif

            safety = 1d-9

            if (nproc == 1 .or. nproc == 6) then ! W+, W-
                npart = 2
                if (gen_res(r,phaseqt,p,pswt) .eqv. .false. ) then
                    resint = 0._dp
                endif
            elseif (any(nproc == [31,32])) then ! Z -> e e
                npart = 2
                ! generate phase space at zero pT
                if (gen_res(r,phaseqt,p,pswt) .eqv. .false. ) then
                    resint = 0._dp
                endif
            elseif (kcase == kggfus0) then ! H -> ga ga
                npart = 2
                !if (gen_res(r,phaseqt,p,pswt) .eqv. .false.) then
                    !resint = 0._dp
                    !return
                !endif
                call gen2m(r,p,pswt,*999)
            elseif (kcase == kHi_Zga) then
                npart=3
                call gen3h(r,p,pswt,*999)
            elseif (kcase == kWHbbar .or. kcase == kWHgaga) then
                npart=4
                call genVH(r,p,pswt,*999)
            elseif (kcase == kZHbbar .or. kcase == kZHgaga) then
                npart=4
                call genVH(r,p,pswt,*999)
            elseif (kcase == kZH__WW) then
                npart=6
                call gen6(r,p,pswt,*999)
            elseif (any(nproc == [300,305])) then ! Z -> e+ e-
                npart = 3
                if (ipsgen == 1) then
                    call gen_Vphotons_jets(r,1,0,p,pswt,*999)
                elseif (ipsgen == 2) then
                    call genVphoton(r,p,pswt,*999)
                else
                  error stop
                endif
                zgaips: block
                    real(dp) :: s34, s345, wt34, wt345, wtips(2)
                    real(dp) :: dot
                  s34 = 2._dp*dot(p,3,4)
                  s345 = s34+2._dp*dot(p,3,5)+2._dp*dot(p,4,5)
                  wt34 = s34*(s34-zmass**2)**2+(zmass*zwidth)**2
                  wt345 = s345*(s345-zmass**2)**2+(zmass*zwidth)**2
                  wtips(1) = wt345
                  wtips(2) = wt34
                  pswt = pswt*wtips(ipsgen)/(wtips(1)+wtips(2))
                end block zgaips
            elseif (nproc == 285 .or. nproc == 2851 .or. nproc == 2852) then ! gamma gamma
                npart = 2
                call gen_photons_jets_res(r,2,0,p,pswt,*999)
                safety = 1d-7
            elseif (kcase == kZZlept) then
                npart = 4
                call gen4(r,p,pswt,*999)
            else
                write (*,*) __FILE__//": undefined nproc, line ", __LINE__
                error stop 
            endif

            if (.not. passed_smallnew(p,npart,safety)) then
                goto 999
            endif


            if (any(ieee_is_nan(p(1:npart,:)) .eqv. .true.)) then
              !write(6,*) 'Discarding NaN or infinite phase space point'
              resint = 0._dp
              return
            endif


    ! these are calculated before the boost
            xx(1)=-2._dp*p(1,4)/sqrts
            xx(2)=-2._dp*p(2,4)/sqrts

            if ( (xx(1) > one)  .or. (xx(2) > one) &
             .or.(xx(1) < xmin) .or. (xx(2) < xmin)) then
                resint = 0._dp
                return
            endif

    ! save before boost for matrix element evaluation
            p_born(1:npart+2,:) = p(1:npart+2,:)
    ! mostly for plotting purposes
            call recoilBoost(qt, phi, p)

            if (boostBorn) then
                p_born = p
            endif

    ! check cuts, fills ptildejet
            includeTaucutgrid(0) = .TRUE. 
            if (includedipole(0,p) .eqv. .FALSE. ) then
                resint = 0._dp
                return
            endif

    ! fill pjet array
            call getptildejet(0,pjet)


            flux = fbGeV2/(two*xx(1)*xx(2)*sqrts**2)

            ! central value
            call computeResint(p, p_born, qt, kresorder, xx, resout=xmsq)

            if (doScalevar .and. bin) then
                do j=1,maxscalevar
                    call computeResint(p,p_born,qt,kresorder,xx, resout=xmsqMu, &
                        muMultHard_in=scalevarmult(j), muMultRes_in=facscalevarmult(j))
                    scalereweight(j) = xmsqMu / xmsq
                enddo

                if (scalevar_rapidity) then
                    do j=1,2
                        scalevar_rapidity_i = j
                        call computeResint(p,p_born,qt,kresorder,xx,resout=xmsqMu)
                        scalereweight(maxscalevar+j) = xmsqMu / xmsq
                        if (ieee_is_nan(scalereweight(maxscalevar+j))) then
                            write (*,*) "NaN in scalevar_rapidity", xmsqMu, xmsq
                        endif
                    enddo
                    scalevar_rapidity_i = 0
                endif
            endif

            if ((maxPDFsets > 0) .and. bin) then
                pdfreweight(:) = 0._dp
                do j=1,maxPDFsets
                    currentPDF = j
                    call computeResint(p, p_born, qt, kresorder, xx, resout=xmsqPdf)
                    pdfreweight(currentPDF) = flux*xjac*pswt*(xmsq-xmsqPdf)/BrnRat*wgt
                enddo
            endif

            resint = flux*xjac*pswt*xmsq/BrnRat

            !write (*,*) "BrnRat = ", BrnRat
            !write (*,*) "resint = ", resint
            !pause

            val=resint*wgt

            if (ieee_is_nan(val) .OR. ( .NOT. ieee_is_finite(val))) then
                write(6,*) 'Discarded NaN, val=',val
                resint = 0._dp
                return
            endif

            if (bin) then
                includeTaucutgrid(0) = .true. 
                call nplotter_new(pjet,val)
            endif
            
            return

! Alternative return
 999        continue
            resint = 0._dp
            return

        end function resint

        subroutine computeResint(p, p_born, qt, order, xx, resout, resexpout, muMultHard_in, muMultRes_in)
            use types
            use qtResummation
            use Scalevar
            use GammaGamma3L
            implicit none
            include 'nf.f'
            include 'mxpart.f'
            include 'kprocess.f'
            include 'constants.f'
            include 'initialscales.f'
            include 'dynamicscale.f'
            include 'scale.f'
            include 'qcdcouple.f'
            include 'ewcharge.f'
            include 'ewcouple.f'

            real(dp), intent(in) :: p(mxpart,4), p_born(mxpart,4)
            real(dp), intent(in) :: qt
            integer, intent(in) :: order
            real(dp), intent(in) :: xx(2)
            real(dp), intent(out), optional :: resout, resexpout
            real(dp), intent(in), optional :: muMultHard_in, muMultRes_in

            real(dp) :: twomass, threemass, fourmass, puremass
            real(dp) :: gg_2gam_msbar

            integer :: j,k
            real(dp) :: muMultHard, muMultRes, hard(2)
            real(dp) :: xmsq, qsq, msq(-nf:nf,-nf:nf), res, resexp

            real(dp) :: msqall(-nf:nf,-nf:nf,0:3), ggcontrib

            integer :: hardorder

            if (present(muMultHard_in)) then
                muMultHard = muMultHard_in
            else
                muMultHard = 1._dp
            endif

            if (present(muMultRes_in)) then
                muMultRes = muMultRes_in
            else
                muMultRes = 1._dp
            endif

            if (dynamicscale) then
                call scaleset(initscale*muMultHard, initfacscale*muMultRes, p)
            else
                call usescales(initscale*muMultHard, initfacscale*muMultRes)
            endif

            msqall = 0._dp

            if (kcase == kW_only) then
                qsq = twomass(1,2,p)**2
                call hardqq(qsq, musq, hard)
                if (boostBorn) then
                    call qqb_w(p, msq)
                else
                    call qqb_w(p_born, msq)
                endif

                where(msq /= 0._dp) msqall(:,:,0) = msq(:,:)
                where(msq /= 0._dp) msqall(:,:,1) = hard(1)*msq(:,:)*(as/2._dp/pi)
                where(msq /= 0._dp) msqall(:,:,2) = hard(2)*msq(:,:)*(as/2._dp/pi)**2

            elseif (kcase == kZ_only) then
                qsq = twomass(1,2,p)**2
                call hardqq(qsq, musq, hard)
                if (boostBorn) then
                    call qqb_z(p, msq)
                else
                    call qqb_z(p_born, msq)
                endif

                where(msq /= 0._dp) msqall(:,:,0) = msq(:,:)
                where(msq /= 0._dp) msqall(:,:,1) = hard(1)*msq(:,:)*(as/2._dp/pi)
                where(msq /= 0._dp) msqall(:,:,2) = hard(2)*msq(:,:)*(as/2._dp/pi)**2

            elseif (kcase == kggfus0) then
                qsq = twomass(1,2,p)**2
                call hardgg(qsq, musq, hard)
                if (boostBorn) then
                    call gg_h(p,msq)
                else
                    call gg_h(p_born, msq)
                endif
                ! Born with full top-loop
                !call finitemtcorr(mtcorr)
                !msq(0,0) = msq(0,0)*mtcorr
                ! debug
                !as = 0.1126394647838897d0

                where(msq /= 0._dp) msqall(:,:,0) = msq(:,:)
                where(msq /= 0._dp) msqall(:,:,1) = hard(1)*msq(:,:)*(as/2._dp/pi)
                where(msq /= 0._dp) msqall(:,:,2) = hard(2)*msq(:,:)*(as/2._dp/pi)**2
            elseif (kcase == kHi_Zga) then
                qsq = threemass(3,4,5,p)**2
                call hardgg(qsq, musq, hard)
                if (boostBorn) then
                    call gg_hzgam(p, msq)
                else
                    call gg_hzgam(p_born, msq)
                endif

                where(msq /= 0._dp) msqall(:,:,0) = msq(:,:)
                where(msq /= 0._dp) msqall(:,:,1) = hard(1)*msq(:,:)*(as/2._dp/pi)
                where(msq /= 0._dp) msqall(:,:,2) = hard(2)*msq(:,:)*(as/2._dp/pi)**2
            elseif (kcase == kWHbbar) then
                qsq = fourmass(3,4,5,6,p)**2
                call hardqq(qsq, musq, hard)
                if (boostBorn) then
                    call qqb_wh(p, msq)
                else
                    call qqb_wh(p_born, msq)
                endif

                where(msq /= 0._dp) msqall(:,:,0) = msq(:,:)
                where(msq /= 0._dp) msqall(:,:,1) = hard(1)*msq(:,:)*(as/2._dp/pi)
                where(msq /= 0._dp) msqall(:,:,2) = hard(2)*msq(:,:)*(as/2._dp/pi)**2
            elseif (kcase == kWHgaga) then
                qsq = fourmass(3,4,5,6,p)**2
                call hardqq(qsq, musq, hard)
                if (boostBorn) then
                    call qqb_wh_gaga(p, msq)
                else
                    call qqb_wh_gaga(p_born, msq)
                endif

                where(msq /= 0._dp) msqall(:,:,0) = msq(:,:)
                where(msq /= 0._dp) msqall(:,:,1) = hard(1)*msq(:,:)*(as/2._dp/pi)
                where(msq /= 0._dp) msqall(:,:,2) = hard(2)*msq(:,:)*(as/2._dp/pi)**2
            elseif (kcase == kZHbbar) then
                qsq = fourmass(3,4,5,6,p)**2
                call hardqq(qsq, musq, hard)
                if (boostBorn) then
                    call qqb_zh(p, msq)
                else
                    call qqb_zh(p_born, msq)
                endif

                where(msq /= 0._dp) msqall(:,:,0) = msq(:,:)
                where(msq /= 0._dp) msqall(:,:,1) = hard(1)*msq(:,:)*(as/2._dp/pi)
                where(msq /= 0._dp) msqall(:,:,2) = hard(2)*msq(:,:)*(as/2._dp/pi)**2
            elseif (kcase == kZH__WW) then
                qsq = puremass(p(3,:)+p(4,:)+p(5,:)+p(6,:)+p(7,:)+p(8,:))**2
                call hardqq(qsq, musq, hard)
                if (boostBorn) then
                    call qqb_zh_ww(p, msq)
                else
                    call qqb_zh_ww(p_born, msq)
                endif

                where(msq /= 0._dp) msqall(:,:,0) = msq(:,:)
                where(msq /= 0._dp) msqall(:,:,1) = hard(1)*msq(:,:)*(as/2._dp/pi)
                where(msq /= 0._dp) msqall(:,:,2) = hard(2)*msq(:,:)*(as/2._dp/pi)**2
            elseif (kcase == kZHgaga) then
                qsq = fourmass(3,4,5,6,p)**2
                call hardqq(qsq, musq, hard)
                if (boostBorn) then
                    call qqb_zh_gaga(p, msq)
                else
                    call qqb_zh_gaga(p_born, msq)
                endif

                where(msq /= 0._dp) msqall(:,:,0) = msq(:,:)
                where(msq /= 0._dp) msqall(:,:,1) = hard(1)*msq(:,:)*(as/2._dp/pi)
                where(msq /= 0._dp) msqall(:,:,2) = hard(2)*msq(:,:)*(as/2._dp/pi)**2
            elseif (kcase == kZgamma) then
                qsq = threemass(3,4,5,p)**2
                if (boostBorn) then
                    call zgam_mat(p,msqall)
                    call gg_zgam(p,ggcontrib)
                else
                    call zgam_mat(p_born,msqall)
                    call gg_zgam(p_born,ggcontrib)
                endif

                msqall(0,0, 2) = ggcontrib
                msq(:,:) = msqall(:,:,0)
            elseif (kcase == kgg2gam) then
                qsq = twomass(3,4,p)**2
                if (boostBorn) then
                    call gg_gamgam(p,ggcontrib)
                    !call gggaga_mt(p,ggcontrib) ! with mt effect
                    msqall(0,0, 0) = ggcontrib
                    if (order >= 3) then
                        msqall(0,0, 1) = gg_2gam_msbar(p)
                    endif
                else
                    call gg_gamgam(p_born,ggcontrib)
                    !call gggaga_mt(p_born,ggcontrib) ! with mt effect
                    msqall(0,0, 0) = ggcontrib
                    if (order >= 3) then
                        msqall(0,0, 1) = gg_2gam_msbar(p_born)
                    endif
                endif

            elseif (kcase == kgamgam) then
                qsq = twomass(3,4,p)**2
                if (boostBorn) then
                    call qqb_gamgam(p,msq)
                else
                    call qqb_gamgam(p_born,msq)
                endif

                where(msq /= 0._dp) msqall(:,:,0) = msq(:,:)

                gamgam: block
                    real(dp) :: qqb
                    real(dp) :: qsum, q2sum, fac
                    integer :: jj

                    real(dp) :: twoloop_nfv2, threeloop_nfv, threeloop_nfv2
                    real(dp) :: hard3(3), tree

                    hard3(:) = 0._dp
                    
                    if (boostBorn) then
                        if (order >= 7) then
                            call gamgamhard(p,tree,hard3,twoloop_nfv2, &
                                            threeloop_nfv, threeloop_nfv2)
                            ! normalize as old routines
                            hard3(:) = hard3(:) / tree

                            ! for debugging:
                            !call gamgamampsq_new(2,p,1,2,3,4,qqb,hard,tlrem)
                        else
                            call gamgamampsq_new(2,p,1,2,3,4,qqb,hard3(1:2),twoloop_nfv2)
                        endif
                    else
                        if (order >= 7) then
                            call gamgamhard(p_born,tree,hard3,twoloop_nfv2, &
                                            threeloop_nfv, threeloop_nfv2)
                            ! normalize as old routines
                            hard3(:) = hard3(:) / tree
                        else
                            call gamgamampsq_new(2,p_born,1,2,3,4,qqb,hard3(1:2),twoloop_nfv2)
                        endif
                    endif

                    !write (*,*) "Comparison ", order
                    !write (*,*) "ONELOOP", hard3(1) / (hard(1))
                    !write (*,*) "DIFF", (hard3(1) - hard(1))
                    !write (*,*) "TWOLOOP", hard3(2) / (hard(2))
                    !write (*,*) "TWOLOOP NFV2", twoloop_nfv2 / tlrem
                    !pause

                    fac = xn*esq**2*0.5_dp
                    q2sum = sum(Q(1:5)**2)
                    qsum = sum(Q(1:5))

                    where(msq /= 0._dp) msqall(:,:,1) = hard3(1)*msq(:,:)*(as/2._dp/pi)

                    if (order >= 5) then
                        where(msq /= 0._dp) msqall(:,:,2) = hard3(2)*msq(:,:)*(as/2._dp/pi)**2

                        do jj=-nf,nf
                            if (jj == 0) cycle
                            msqall(jj,-jj,2) = msqall(jj,-jj,2) + &
                                fac*aveqq*twoloop_nfv2 * &
                                q2sum*Q(abs(jj))**2 * (as/2._dp/pi)**2
                        enddo
                    endif

                    if (order >= 7) then
                        where(msq /= 0._dp) msqall(:,:,3) = hard3(3)*msq(:,:)*(as/2._dp/pi)**3

                        do jj=-nf,nf
                            if (jj == 0) cycle

                            msqall(jj,-jj,3) = msqall(jj,-jj,3) + &
                                fac*aveqq*threeloop_nfv2 * &
                                q2sum*Q(abs(jj))**2 * (as/2._dp/pi)**3

                            msqall(jj,-jj,3) = msqall(jj,-jj,3) + &
                                fac*aveqq*threeloop_nfv * &
                                qsum*Q(abs(jj))**2 * (as/2._dp/pi)**3
                        enddo
                endif

            end block gamgam

            !! gg contributions

            !ggcontrib = 0._dp
            !if (boostBorn) then
                !if (order >= 5) then
                    !call gg_gamgam(p,ggcontrib)
                    !!call gggaga_mt(p,ggcontrib) ! with mt effect
                    !msqall(0,0, 2) = ggcontrib
                !endif
                !if (order >= 7) then
                    !msqall(0,0, 3) = gg_2gam_msbar(p)
                !endif
            !else
                !if (order >= 5) then
                    !call gg_gamgam(p_born,ggcontrib)
                    !!call gggaga_mt(p_born,ggcontrib) ! with mt effect
                !endif
                !if (order >= 7) then
                    !msqall(0,0, 3) = gg_2gam_msbar(p_born)
                !endif
            !endif
            

        elseif (kcase == kZZlept) then
            qsq = fourmass(3,4,5,6,p)**2
            if (boostBorn) then
                call qqb_zz(p,msq)
            else
                call qqb_zz(p_born,msq)
            endif
            where(msq /= 0._dp) msqall(:,:,0) = msq(:,:)

            !msq = 0._dp
            !call qqb_zz_v(p,msq)
            !where(msq /= 0._dp) msqall(:,:,1) = msq(:,:)
        else
            write (*,*) __FILE__//": undefined kcase, line", __LINE__
            error stop
        endif


        if (order < 3) then
           hardorder = 0
        elseif (order < 5) then
           hardorder = 1
        elseif (order < 7) then
            hardorder = 2
        else
           hardorder = 3
        endif

        if (present(resout)) then
            xmsq = 0._dp
            do j=-5,5; do k =-5,5
                if ( all(msqall(j,k,0:hardorder) == 0._dp) ) cycle
                res = 0._dp

                ! hard function set to 0._dp, not required for resummed piece
                call resummation(qsq, qt, xx(1), xx(2), j, k, 0._dp, sqrt(musq), &
                    order, muMult=muMultRes, res=res)

                if (hardorder == 0) then
                   xmsq = xmsq + res * msqall(j,k,0)
                elseif (hardorder == 1) then
                    xmsq = xmsq + res * sum(msqall(j,k,[0,1]))
                elseif (hardorder == 2) then
                    xmsq = xmsq + res * sum(msqall(j,k,[0,1,2]))
                elseif (hardorder == 3) then
                    xmsq = xmsq + res * sum(msqall(j,k,[0,1,2,3]))
                    !write (*,*) j,k, msqall(j,k,3)/sum(msqall(j,k,[0,1,2]))
                endif
            enddo; enddo

            resout = xmsq
            return
        endif

        if (present(resexpout)) then
            xmsq = 0._dp
            do j=-5,5; do k =-5,5
                ! cycle if no contribution through NLO
                if ( all(msqall(j,k,[0,1]) == 0._dp) ) cycle

                resexp = 0._dp

                ! coefficient in as/4/pi
                hard(1) = msqall(j,k,1) / msqall(j,k,0) / (as/4._dp/pi)

                call resummation(qsq, qt, xx(1), xx(2), j, k, hard(1), sqrt(musq), order, resexp=resexp)

                ! matching correction
                xmsq = xmsq - msqall(j,k,0)*resexp

                !write (*,*) "prefactor", j,k, msqall(j,k,0)*fbgev2
            enddo; enddo

            resexpout = xmsq
            return
        endif


    end subroutine computeResint

    function resexpint(r,wgt)
        use ieee_arithmetic
        use types
        use Scalevar
        use PDFerrors
        use SCET
        use MCFMStorage
        use qtResummation
        use LHAPDF
        use MCFMSetupPlots
        implicit none
        include 'constants.f'
        include 'mxpart.f'
        include 'vegas_common.f'
        include 'kprocess.f'
        include 'kpart.f'
        include 'energy.f'
        include 'npart.f'
        include 'dynamicscale.f'
        include 'initialscales.f'
        include 'scalevar.f'
        include 'scale.f'
        include 'facscale.f'
        include 'qcdcouple.f'
        include 'couple.f'
        include 'nlooprun.f'
        include 'x1x2.f'
        include 'xmin.f'
        include 'nf.f'
        include 'nflav.f'
        include 'nwz.f'
        include 'nproc.f'
        include 'ipsgen.f'
        include 'masses.f'
        include 'beamtype.f'
        logical:: bin
        common/bin/bin
        real(dp) :: BrnRat
        common/BrnRat/BrnRat

        real(dp) :: resexpint
        real(dp), intent(in) :: r(mxdim), wgt

        real(dp):: p(mxpart,4),pjet(mxpart,4),xmsq
        real(dp) :: val,pswt,flux

        real(dp) :: p_born(mxpart,4)

        ! functions
        logical :: includedipole
        real(dp) :: xjac
        real(dp) :: qt, phi
        real(dp) :: xmsqMu, xmsqPdf
        real(dp) :: phaseqt

        real(dp) :: pt34com
        common/pt34com/pt34com
!$omp threadprivate(/pt34com/)

        integer :: j

        resexpint = 0._dp

        currentPDF = 0
        currentNd = 0

        if (doScalevar .and. bin) then
            scalereweight(:) = 1._dp
        endif

        p(:,:) = 0._dp
        pjet(:,:) = 0._dp

        qt = r(ndim)*(qtmaxResexp-qtminResexp) + qtminResexp
        phi = 2._dp*pi*r(ndim-1)
        xjac = 2._dp*qt*(qtmaxResexp-qtminResexp)

!       ! for debugging purposes we can fix qt here
        !qt = 1.5_dp
        !xjac = 2._dp*qt

        pt34com = qt

        if (qt > qtmaxResexp) then
            resexpint = 0._dp
            return
        endif

        if (qt < qtminResexp) then
            resexpint = 0._dp
            return
        endif

        p(:,:) = 0._dp

        if (phaseUseQt) then
            phaseqt = qt
        else
            phaseqt = 0._dp
        endif

        if (nproc == 1 .or. nproc == 6) then
            npart = 2
            if (gen_res(r,phaseqt,p,pswt) .eqv. .false. ) then
                resexpint = 0._dp
            endif
        elseif (any(nproc == [31,32])) then ! Z -> e e
            npart = 2
            ! generate phase space at zero pT
            if (gen_res(r,phaseqt,p,pswt) .eqv. .false. ) then
                resexpint = 0._dp
            endif
        elseif (kcase == kggfus0) then ! H -> ga ga
            npart = 2
            if (gen_res(r,phaseqt,p,pswt) .eqv. .false.) then
                resexpint = 0._dp
                return
            endif
        elseif (kcase == kHi_Zga) then
            npart=3
            call gen3h(r,p,pswt,*888)
        elseif (kcase == kWHbbar .or. kcase == kWHgaga) then
            npart=4
            call genVH(r,p,pswt,*888)
        elseif (kcase == kZHbbar .or. kcase == kZHgaga) then
            npart=4
            call genVH(r,p,pswt,*888)
        elseif (kcase == kZH__WW) then
            npart=6
            call gen6(r,p,pswt,*888)
        elseif (any(nproc == [300,305])) then ! Z -> e+ e-
            npart = 3
            if (ipsgen == 1) then
                call gen_Vphotons_jets(r,1,0,p,pswt,*888)
            elseif (ipsgen == 2) then
                call genVphoton(r,p,pswt,*888)
            else
              error stop
            endif
            zgaips: block
                real(dp) :: s34, s345, wt34, wt345, wtips(2)
                real(dp) :: dot
              s34 = 2._dp*dot(p,3,4)
              s345 = s34+2._dp*dot(p,3,5)+2._dp*dot(p,4,5)
              wt34 = s34*(s34-zmass**2)**2+(zmass*zwidth)**2
              wt345 = s345*(s345-zmass**2)**2+(zmass*zwidth)**2
              wtips(1) = wt345
              wtips(2) = wt34
              pswt = pswt*wtips(ipsgen)/(wtips(1)+wtips(2))
            end block zgaips
        elseif (nproc == 285 .or. nproc == 2851 .or. nproc == 2852) then
            npart = 2
            call gen_photons_jets_res(r,2,0,p,pswt,*888)
        elseif (kcase == kZZlept) then
            npart = 4
            call gen4(r,p,pswt,*888)
        else
            write (*,*) __FILE__//": undefined nproc, line ", __LINE__
            error stop 
        endif

        !call smallnew(p,npart,*999)
        if (.false.) then
            goto 888
        endif


        if (any(ieee_is_nan(p(1:npart+2,:)) .eqv. .true.)) then
          !write(6,*) 'Discarding NaN or infinite phase space point'
          resexpint = 0._dp
          return
        endif

! these are calculated before the boost
        xx(1)=-2._dp*p(1,4)/sqrts
        xx(2)=-2._dp*p(2,4)/sqrts

        if ( (xx(1) > one)  .or. (xx(2) > one) &
         .or.(xx(1) < xmin) .or. (xx(2) < xmin)) then
            resexpint = 0._dp
            return
        endif

! save before boost for matrix element evaluation
        p_born(1:npart+2,:) = p(1:npart+2,:)
! mostly for plotting purposes
        call recoilBoost(qt, phi, p)

        if (boostBorn) then
            p_born = p
        endif

! check cuts, fills ptildejet
        includeTaucutgrid(0) = .TRUE. 
        if (includedipole(0,p) .eqv. .FALSE. ) then
            resexpint = 0._dp
            return
        endif

        call storeptilde(0,p)

! fill pjet array
        call getptildejet(0,pjet)

        if (dynamicscale) then
            call scaleset(initscale, initfacscale, p)
        else
            call usescales(initscale, initfacscale)
        endif

        flux = fbGeV2/(two*xx(1)*xx(2)*sqrts**2)

        ! central value
        xmsq = 0._dp
        call computeResint(p, p_born, qt, kresorder, xx, resexpout=xmsq)

        if (doScalevar .and. bin) then
            do j=1,maxscalevar
                call computeResint(p,p_born,qt,kresorder,xx, resexpout=xmsqMu, &
                    muMultHard_in=scalevarmult(j), muMultRes_in=facscalevarmult(j))
                scalereweight(j) = xmsqMu / xmsq
            enddo
        endif

        if ((maxPDFsets > 0) .and. bin) then
            pdfreweight(:) = 0._dp
            do j=1,maxPDFsets
                currentPDF = j
                call computeResint(p, p_born, qt, kresorder, xx, resexpout=xmsqPdf)
                pdfreweight(currentPDF) = flux*xjac*pswt*(xmsq-xmsqPdf)/BrnRat*wgt
            enddo
        endif

        resexpint = flux*xjac*pswt*xmsq/BrnRat

        val=resexpint*wgt

        if (ieee_is_nan(val) .OR. ( .NOT. ieee_is_finite(val))) then
            !write(6,*) 'Discarded NaN, val=',val
            resexpint = 0._dp
            return
        endif
              
        if (bin) then
            includeTaucutgrid(0) = .true. 
            call nplotter_new(pjet,val)
        endif
        
        return

! Alternative return
 888    continue
        resexpint = 0._dp
        return

    end function resexpint

end module
