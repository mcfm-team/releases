!
!  SPDX-License-Identifier: GPL-3.0-or-later
!  Copyright (C) 2019-2022, respective authors of MCFM.
!
 
module nplotter_ZZ
      use MCFMPlotting
      use ResummationTransition, only: transition
      implicit none

      public :: setup, book
      private

      integer, save, allocatable :: histos(:)

      contains

      subroutine setup()
          use types
          implicit none

          allocate(histos(3))

          histos(1) = plot_setup_uniform(0._dp,20._dp,0.1_dp,'pt3456')
          histos(2) = plot_setup_uniform(0._dp,500._dp,10._dp,'m3456')
          histos(3) = plot_setup_uniform(0._dp,500._dp,10._dp,'m34m56')

      end subroutine

      subroutine book(p,wt,ids,vals,wts)
          use types
          implicit none
          include 'mxpart.f'
          include 'kpart.f'
          include 'taucut.f'! abovecut
          include 'constants.f'

          real(dp), intent(in) :: p(mxpart,4)
          real(dp), intent(in) :: wt
          
          integer, allocatable, intent(out) :: ids(:)
          real(dp), allocatable, intent(out) :: vals(:)
          real(dp), allocatable, intent(out) :: wts(:)
          real(dp) :: pt3456
          real(dp) :: ptfour
          real(dp) :: puremass, mZZ,mZmZ

          pt3456 = ptfour(3,4,5,6,p)
          mZmZ = puremass(p(3,:)+p(4,:)) + puremass(p(5,:)+p(6,:))
          mZZ = puremass(p(3,:)+p(4,:)+p(5,:)+p(6,:))

          ids = histos
          vals = [pt3456,mZZ,mZmZ]
          wts = [wt,wt,wt]

      end subroutine

end module
