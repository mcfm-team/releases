!
!  SPDX-License-Identifier: GPL-3.0-or-later
!  Copyright (C) 2019-2022, respective authors of MCFM.
!
      function dot(p,i,j)
      implicit none
      include 'types.f'
      real(dp):: dot
      include 'mxpart.f'
      integer:: i,j
      real(dp):: p(mxpart,4)
      dot=p(i,4)*p(j,4)-p(i,1)*p(j,1)-p(i,2)*p(j,2)-p(i,3)*p(j,3)
      return
      end

      pure function dotvec(pi,pj)
      implicit none
      include 'types.f'
      real(dp) :: dotvec
      real(dp), intent(in) :: pi(4), pj(4)

      dotvec=pi(4)*pj(4)-pi(1)*pj(1)-pi(2)*pj(2)-pi(3)*pj(3)
      return
      end function

      pure function massvec(p)
        implicit none
        include 'types.f'

        real(dp) :: massvec
        real(dp), intent(in) :: p(4)

        massvec=p(4)*p(4)-p(1)*p(1)-p(2)*p(2)-p(3)*p(3)
      end


      function dotpr(p,q)
      use types
      implicit none
      real(dp):: dotpr
c---   returns the dot product of the two four vectors p(4) and q(4)

      real(dp):: p(4),q(4)

      dotpr=p(4)*q(4)-p(1)*q(1)-p(2)*q(2)-p(3)*q(3)

      return
      end function dotpr

