!
!  SPDX-License-Identifier: GPL-3.0-or-later
!  Copyright (C) 2019-2022, respective authors of MCFM.
!
      subroutine read_jetcuts(read_ptmin,read_etamin,read_etamax)
      implicit none
      include 'types.f'
      include 'jetcuts.f'
      real(dp):: read_ptmin,read_etamin,read_etamax

      read_ptmin=ptjetmin
      read_etamin=etajetmin
      read_etamax=etajetmax

      return
      end



