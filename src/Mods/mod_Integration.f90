!
!  SPDX-License-Identifier: GPL-3.0-or-later
!  Copyright (C) 2019-2022, respective authors of MCFM.
!
 
module Integration
    use types
    implicit none

    enum, bind(c)
          enumerator :: nloReal=1, nloVirt=2, nnloBelow=3, &
               nnloVirtAbove=4, nnloRealAbove=5, &
               snloBelow = 6, snloAbove = 7, lord=8, nloFrag=9, &
               nloRealExtra = 10, nloResummed=11, nloResAbove = 12, &
               nnloResVirtAbove=13, nnloResRealAbove=14, nloResexp = 15
    endenum

    ! adjust maxParts to be the maximum number used above
    integer, parameter :: maxParts = 15

#define SINGLETOP_NNLO_REALCHANNELS 12
#if SINGLETOP_NNLO_REALCHANNELS == 66
    integer, parameter :: maxIPS = 73
#else
    integer, parameter :: maxIPS = 19
#endif

    integer, parameter :: ndim_incr(maxParts) = [3,1,2,1,3,2,0,0,1,3,2,0,1,3,2]

    character(*), parameter :: partStrings(maxParts) = [ &
        "NLO real           ", &
        "NLO virt           ", &
        "NNLO below cut     ", &
        "NNLO virt above cut", &
        "NNLO real above cut", &
        "SNLO below cut     ", &
        "SNLO above cut     ", &
        "LO                 ", &
        "NLO frag           ", &
        "NLO real extra     ", &
        "Resummed           ", &
        "NLO Resummed above ", &
        "NNLO Res. virtabove", &
        "NNLO Res. realabove", &
        "Res. f.o. expansion"]

    real(dp), save :: warmupChisqGoal

end module
