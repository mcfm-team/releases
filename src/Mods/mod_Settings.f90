!
!  SPDX-License-Identifier: GPL-3.0-or-later
!  Copyright (C) 2019-2022, respective authors of MCFM.
!
 
module MCFMSettings
      implicit none

      logical, public, save :: newStyleHistograms = .false.

end module
