The purpose of the files in this directory is the NLO calculation of the process
	q + qbar -> W + b + b~

with massless b quarks.

R. K. Ellis and S. Veseli,
``Strong radiative corrections to W b anti-b production in p anti-p collisions,''
Phys. Rev. D 60, 011501 (1999)
[arXiv:hep-ph/9810489 [hep-ph]].

J. M. Campbell,
``$W/Z + B\bar{B}$ / Jets at NLO Using the Monte Carlo MCFM,''
[arXiv:hep-ph/0105226 [hep-ph]].

