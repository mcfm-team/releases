The purpose of the files in this directory is the NLO calculation of the process
	p + p -> Z + \gamma

J. M. Campbell, R. K. Ellis and C. Williams,
``Vector boson pair production at the LHC,''
JHEP 07, 018 (2011)
[arXiv:1105.0020 [hep-ph]].

