!
!  SPDX-License-Identifier: GPL-3.0-or-later
!  Copyright (C) 2019-2022, respective authors of MCFM.
!
 
select case (powAs)
    case (0)
        select case (powLperp)
            case (0)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (1)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (2)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (3)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case default
                Ibar_select = 0._dp
        end select
    case (1)
        select case (powLperp)
            case (0)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (1)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (2)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (3)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case default
                Ibar_select = 0._dp
        end select
    case (2)
        select case (powLperp)
            case (0)
                select case (type_in)
                    case (1)
                        Ibar_select = z**3*(-482.71622869222256_dp) + z**4*(-60.49891979174238_dp) + (-6.575689489654321_dp) + &
                        z**5*(1.668274502516088_dp) + (2.6451727005890224_dp)/z + z*(7.993737974147562_dp) + &
                        z**2*(537.483652803029_dp) + (z*(-6.666666666666666_dp) + (1.3333333333333333_dp) + &
                        z**2*(213.40596468377385_dp) + z**3*(392.525592453522_dp))*Log(z) + (z**3*(-68.67155311692338_dp) + &
                        z*(-1.5555555555555554_dp) + (-0.6666666666666666_dp) + z**2*(27.80137308718255_dp))*Log(z)**2 + &
                        (z*(0.2962962962962963_dp) + (0.5925925925925926_dp) + z**2*(1.7401934484961763_dp) + &
                        z**3*(23.858585392707216_dp))*Log(z)**3

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (1)
                select case (type_in)
                    case (1)
                        Ibar_select = z**2*(-40.60694027360694_dp) + z*(-13.054680678027468_dp) + (-7.703703703703703_dp)/z + &
                        z**5*(-0.627328431372549_dp) + z**4*(1.8567738905846443_dp) + (11.297892423978965_dp) + &
                        z**3*(48.83798581122764_dp) + (z**3*(-36.29506406523005_dp) + z**2*(-17.27835736192615_dp) + &
                        z*(-9.777777777777779_dp) + (-0.8888888888888888_dp))*Log(z) + (z**2*(0.16551702514579544_dp) + &
                        z*(1.7777777777777777_dp) + (3.5555555555555554_dp) + z**3*(9.853247118358167_dp))*Log(z)**2

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (2)
                select case (type_in)
                    case (1)
                        Ibar_select = z**2*(-1.7777777777777777_dp) + z*(-1.3333333333333333_dp) + (1.3333333333333333_dp) + &
                        (1.7777777777777777_dp)/z + ((2.6666666666666665_dp) + z*(2.6666666666666665_dp))*Log(z)

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (3)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case default
                Ibar_select = 0._dp
        end select
    case (3)
        select case (powLperp)
            case (0)
                select case (type_in)
                    case (1)
                        Ibar_select = (z**3*(-3.4341099262359136e8_dp) + z**6*(-39560.364185309394_dp) + &
                        z**2*(-3295.3625557732435_dp) + (-472.185801152152_dp) + z*(504.2200309897908_dp) + &
                        Nf*(z**4*(-4943.92914118205_dp) + z*(-57.61279662683297_dp) + z**6*(-16.560320644171973_dp) + &
                        (5.88282387997262_dp) + z**2*(50.50110896509391_dp) + z**5*(687.1501547509737_dp) + &
                        z**3*(4274.56817078873_dp)) + z**5*(3.2795734938256973e6_dp) + z**4*(3.4017424282250994e8_dp))/z + &
                        (z**3*(-2.0680195860704923e8_dp) + z**2*(-1.398157629035882e8_dp) + (-410.79394755522935_dp) + &
                        z*(-318.5923450744127_dp) + (-78.98467140066984_dp)/z + Nf*((-35.457848078043234_dp) + &
                        z*(-0.515555083643898_dp) + z**2*(1433.424565505143_dp) + z**3*(2183.0567985987045_dp)))*Log(z) + &
                        (z**2*(-2.4322447525880173e7_dp) + z*(-133.9743881253827_dp) + (106.97016215684792_dp) + &
                        Nf*(z**3*(-1186.0167155711883_dp) + (-13.966979547350224_dp) + z*(-3.9053004732635905_dp) + &
                        z**2*(180.08534114311618_dp)) + z**3*(5.4647073401290886e7_dp))*Log(z)**2 + (z**3*(-9.882034596600426e6_dp) &
                        + z**2*(-2.264770837914974e6_dp) + (-36.21099191941935_dp) + z*(-17.07029459145344_dp) + &
                        Nf*((-2.5569272976680386_dp) + z*(-0.7352537722908093_dp) + z**2*(9.383835657232206_dp) + &
                        z**3*(69.21788306131498_dp)))*Log(z)**3 + (z**2*(-113156.93113263487_dp) + Nf*(z**3*(-37.21414424673223_dp) &
                        + (-0.3786008230452675_dp) + z*(-0.11522633744855965_dp) + z**2*(-0.056455745460574316_dp)) + &
                        z*(0.8641975308641978_dp) + (7.506172839506172_dp) + z**3*(903434.533209043_dp))*Log(z)**4 + &
                        (z**3*(-86524.9654140194_dp) + z**2*(-2437.510647811278_dp) + (-0.45925925925925926_dp) + &
                        z*(1.288888888888889_dp))*Log(z)**5 + ((-2795.883307118071_dp) + z**2*(-1070.5460224804667_dp) + &
                        z**3*(-878.5694657716556_dp) + Nf*(z**2*(-7.379410689810273_dp) + (-5.030944165540118_dp) + &
                        z**3*(3.083698282711209_dp) + z*(9.326656572639182_dp)) + z*(4744.998795370194_dp))*Log(z*(-1._dp) + &
                        (1._dp)) + (z*(-12517.727530004448_dp) + z**3*(-4430.35070098663_dp) + Nf*(z*(-1.1444946128399929_dp) + &
                        z**3*(0.030580661136560605_dp) + (0.3406240567413631_dp) + z**2*(0.773289894962069_dp)) + &
                        (4046.7576591163565_dp) + z**2*(12901.320571874721_dp))*Log(z*(-1._dp) + (1._dp))**2 + &
                        (z*(-42.897419068067926_dp) + z**3*(-13.159651273668437_dp) + Nf*(z**2*(-0.6827091430256285_dp) + &
                        (-0.32694334958049026_dp) + z**3*(0.22726561277195179_dp) + z*(0.7823868798341669_dp)) + &
                        (15.140488835471352_dp) + z**2*(40.91658150626502_dp))*Log(z*(-1._dp) + (1._dp))**3 + &
                        (z**2*(-7.504455857694885_dp) + (-2.2065175424656394_dp) + z**3*(2.525512367491166_dp) + &
                        z*(7.185461032669358_dp))*Log(z*(-1._dp) + (1._dp))**4

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (1)
                select case (type_in)
                    case (1)
                        Ibar_select = (z**3*(-4.137570544800112e6_dp) + z**5*(-143224.0584354666_dp) + z*(-455.71095448386416_dp) + &
                        z**2*(-216.886626563896_dp) + (483.9673467888446_dp) + Nf*(z**3*(-1464.9706389304206_dp) + &
                        z*(-53.202465875626004_dp) + z**6*(-5.3557525871830025_dp) + (-1.1565265637483262_dp) + &
                        z**2*(56.37673348167625_dp) + z**5*(187.3190234295046_dp) + z**4*(1280.9896270460613_dp)) + &
                        z**6*(3284.794493857353_dp) + z**4*(4.277698438734031e6_dp))/z + (z**3*(-2.3497334571323595e6_dp) + &
                        z**2*(-1.651476910119308e6_dp) + z*(-248.1425173541941_dp) + Nf*(z**3*(-1081.7001034429313_dp) + &
                        z**2*(-555.5684605091913_dp) + (-38.14068756693837_dp) + z*(-8.562572247989484_dp)) + &
                        (98.11244277743864_dp)/z + (497.14886135146355_dp))*Log(z) + (z**2*(-267989.4145365464_dp) + &
                        (-117.43267732966508_dp) + z*(-33.550538103564136_dp) + Nf*(z**2*(-80.05862177563473_dp) + &
                        (-11.45679012345679_dp) + z*(-5.135802469135802_dp) + z**3*(176.27122444255033_dp)) + &
                        z**3*(750337.2465155071_dp))*Log(z)**2 + (z**3*(-90958.87649645458_dp) + z**2*(-21011.775038243_dp) + &
                        z*(-13.234567901234568_dp) + Nf*(z**3*(-67.89203291934805_dp) + z**2*(-4.974774569535159_dp) + &
                        (-1.7777777777777777_dp) + z*(-0.5925925925925926_dp)) + (36.345679012345684_dp))*Log(z)**3 + &
                        (z**2*(-676.3207898614029_dp) + (-2.9382716049382718_dp) + z*(8.271604938271604_dp) + &
                        z**3*(14692.230963005815_dp))*Log(z)**4 + (z**2*(-1447.6040371123736_dp) + (-544.4791642015681_dp) + &
                        Nf*(z*(-4.559282020884582_dp) + z**3*(-0.17257441490570324_dp) + (0.7118723215079577_dp) + &
                        z**2*(4.019984114282327_dp)) + z**3*(437.7395614033088_dp) + z*(1554.3436399106329_dp))*Log(z*(-1._dp) + &
                        (1._dp)) + (z*(-74.74156853609831_dp) + z**3*(-21.393301000303122_dp) + Nf*(z**2*(-3.202431556813283_dp) + &
                        (-1.9599822537344245_dp) + z**3*(1.0656690959838735_dp) + z*(4.096744714563834_dp)) + &
                        (31.340800434564265_dp) + z**2*(64.79406910183718_dp))*Log(z*(-1._dp) + (1._dp))**2 + &
                        (z**2*(-10.102286050707095_dp) + (-1.9908893803281575_dp) + z**3*(3.3149575944487277_dp) + &
                        z*(8.778217836586524_dp))*Log(z*(-1._dp) + (1._dp))**3

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (2)
                select case (type_in)
                    case (1)
                        Ibar_select = (z**4*(-9731.273258243622_dp) + z**5*(-887.5293603025209_dp) + (-220.95532436198914_dp) + &
                        z**2*(-74.60853927232975_dp) + z**6*(11.140806991105725_dp) + Nf*(z**4*(-33.032735542313304_dp) + &
                        z*(-16.343545776367677_dp) + z**5*(-2.0156321469592604_dp) + z**6*(0.5980532083920125_dp) + &
                        (1.1851851851851851_dp) + z**2*(8.222169068877047_dp) + z**3*(41.38650653945485_dp)) + &
                        z*(251.62454557814533_dp) + z**3*(10651.601134270764_dp))/z + ((-137.5464974390064_dp) + &
                        z*(-116.44444444444444_dp) + (-46.22222222222222_dp)/z + Nf*((-8.88888888888889_dp) + &
                        z*(-6.518518518518518_dp) + z**2*(8.687208138840761_dp) + z**3*(24.043713577571747_dp)) + &
                        z**2*(3991.944090625632_dp) + z**3*(7496.693974079974_dp))*Log(z) + (z**3*(-1490.003176703887_dp) + &
                        z*(-25.185185185185187_dp) + Nf*(z**3*(-6.0637677494352396_dp) + (-2.3703703703703702_dp) + &
                        z*(-1.1851851851851851_dp) + z**2*(-0.15664547357115216_dp)) + (49.185185185185176_dp) + &
                        z**2*(576.4586405141953_dp))*Log(z)**2 + ((-5.135802469135802_dp) + z*(14.617283950617283_dp) + &
                        z**2*(32.0389454382639_dp) + z**3*(441.5034897636785_dp))*Log(z)**3 + (z*(-177.77963268425538_dp) + &
                        z**3*(-30.466535499847346_dp) + Nf*(z**2*(-3.511929408639212_dp) + (-2.9291277214703895_dp) + &
                        z**3*(1.1802897324733002_dp) + z*(5.260767397636301_dp)) + (87.4343263699818_dp) + &
                        z**2*(120.81184181412095_dp))*Log(z*(-1._dp) + (1._dp))

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (3)
                select case (type_in)
                    case (1)
                        Ibar_select = (-50.62385710459352_dp) + z**4*(-4.407436322826194_dp) + z**3*(-2.6864439571778687_dp) + &
                        z**2*(-0.5265500760136046_dp) + Nf*((-0.7901234567901235_dp)/z + (-0.5925925925925926_dp) + &
                        z*(0.5925925925925926_dp) + z**2*(0.7901234567901235_dp)) + z**5*(1.019060773480663_dp) + &
                        z*(18.11411506631996_dp) + (39.11111111111111_dp)/z + (z**2*(-8.93517064115379_dp) + &
                        Nf*((-1.1851851851851851_dp) + z*(-1.1851851851851851_dp)) + z**3*(-0.866976222063479_dp) + &
                        (7.111111111111111_dp)/z + z*(9.481481481481486_dp) + (19.55555555555555_dp))*Log(z) + &
                        ((-2.9629629629629624_dp) + z**2*(-0.2623712088231026_dp) + z**3*(2.862027514358221_dp) + &
                        z*(13.037037037037035_dp))*Log(z)**2 + (z**2*(-19.900933497371888_dp) + (-16.598390495119958_dp) + &
                        z**3*(6.688308538163001_dp) + z*(29.81101545432885_dp))*Log(z*(-1._dp) + (1._dp))

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case default
                Ibar_select = 0._dp
        end select
    case default
        Ibar_select = 0._dp
end select
