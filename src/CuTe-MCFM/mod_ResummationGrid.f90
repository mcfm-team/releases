!
!  SPDX-License-Identifier: GPL-3.0-or-later
!  Copyright (C) 2019-2022, respective authors of MCFM.
!
 
module qtResummationGrids
      use ieee_arithmetic
      use types
      use LHAPDF
      use PDFerrors
      implicit none

      public :: readAllGrids

      private

    contains

    subroutine readAllGrids()
        use Beamfunctions3L, only: getbeam, getbeam2
        use parseinput
        use iso_fortran_env
#ifdef HAVE_MPI
        use mpi
#endif
        implicit none
#if defined(HAVE_MPI) || defined(HAVE_COARRAY)
        include 'mpicommon.f'
        integer curPDF, n
#endif

        integer :: numpdfmembers, pdfentry
        integer :: i,j,k,l,m
        integer :: ierr,iread

        character(len=255) :: dir = "/data/tneumann/local/share/LHAPDF/"
        character(len=255) :: outdir = '/data/tneumann/grids/'
        character(len=512) :: path, imsg, outpath

        character(len=255) :: dummy
        character(len=5000) :: nextline


#ifdef HAVE_COARRAY
        real(dp), allocatable :: pdfvals(:,:,:,:)[:]
        logical(atomic_logical_kind), allocatable :: work_done(:)[:]
        logical(atomic_logical_kind), save :: prev[*]
        real(dp), save :: xvals(512)[*]
        real(dp), save :: q2vals(512)[*]
        integer, save :: flavors(13)[*]
        integer, save :: curPDF[*]
#else
        real(dp) :: xvals(512)
        real(dp) :: q2vals(512)
        real(dp), allocatable :: pdfvals(:,:,:,:)
        integer :: flavors(13)
#endif


#ifdef HAVE_COARRAY
        integer, save :: numx[*], numq2[*], numflavors[*]
        integer :: nimg
#else
        integer :: numx, numq2, numflavors
#endif
        integer, save :: fl
!$omp threadprivate(fl)

#if defined(HAVE_COARRAY)

        if (this_image() /= 1) then
            write (*,*) "Worker on image ", this_image(), "launched"

            do while (.true.)
                sync all ! sync numx, numq2, numflavors
                currentPDF = curPDF
                if (allocated(pdfvals)) deallocate(pdfvals)
                allocate(pdfvals(11,numflavors,numq2,numx)[*], source=huge(0._dp))
                if (allocated(work_done)) deallocate(work_done)
                allocate(work_done(numx)[*], source=.false.)

                do j=1,numx
                    call atomic_cas(work_done(j)[1], prev, .false., .true.)
                    if (prev .eqv. .false.) then
                        !write (*,*) "Image ", this_image(), " got work x = ", xvals(j)
!$omp parallel do collapse(2)
                        do l=1,numq2
                            do m=1,numflavors
                                fl = flavors(m)
                                if (fl == 21) fl = 0
                                ! we hardcode ih=1 and beam=1
                                ! this means no pdfchannel selection should be done in the input file
                                ! for this grid generation
                                pdfvals(1,m,l,j)[1] =  getbeam(1, fl, 0, 0, xvals(j), q2vals(l))*xvals(j)
                                pdfvals(2,m,l,j)[1] =  getbeam(1, fl, 1, 0, xvals(j), q2vals(l))*xvals(j)
                                pdfvals(3,m,l,j)[1] =  getbeam(1, fl, 1, 1, xvals(j), q2vals(l))*xvals(j)

                                pdfvals(4,m,l,j)[1] =  getbeam(1, fl, 2, 0, xvals(j), q2vals(l))*xvals(j)
                                pdfvals(5,m,l,j)[1] =  getbeam(1, fl, 2, 1, xvals(j), q2vals(l))*xvals(j)
                                pdfvals(6,m,l,j)[1] =  getbeam(1, fl, 2, 2, xvals(j), q2vals(l))*xvals(j)

                                pdfvals(7,m,l,j)[1] =  getbeam(1, fl, 3, 0, xvals(j), q2vals(l))*xvals(j)
                                pdfvals(8,m,l,j)[1] =  getbeam(1, fl, 3, 1, xvals(j), q2vals(l))*xvals(j)
                                pdfvals(9,m,l,j)[1] =  getbeam(1, fl, 3, 2, xvals(j), q2vals(l))*xvals(j)
                                pdfvals(10,m,l,j)[1] =  getbeam(1, fl, 3, 3, xvals(j), q2vals(l))*xvals(j)

                                pdfvals(11,m,l,j)[1] = getbeam2(1, fl, 1, 0, xvals(j), q2vals(l))*xvals(j)

                                if (ieee_is_nan(pdfvals(1,m,l,j)[1])) pdfvals(1,m,l,j)[1] = 0._dp
                                if (ieee_is_nan(pdfvals(2,m,l,j)[1])) pdfvals(2,m,l,j)[1] = 0._dp
                                if (ieee_is_nan(pdfvals(3,m,l,j)[1])) pdfvals(3,m,l,j)[1] = 0._dp
                                if (ieee_is_nan(pdfvals(4,m,l,j)[1])) pdfvals(4,m,l,j)[1] = 0._dp
                                if (ieee_is_nan(pdfvals(5,m,l,j)[1])) pdfvals(5,m,l,j)[1] = 0._dp
                                if (ieee_is_nan(pdfvals(6,m,l,j)[1])) pdfvals(6,m,l,j)[1] = 0._dp
                                if (ieee_is_nan(pdfvals(7,m,l,j)[1])) pdfvals(7,m,l,j)[1] = 0._dp
                                if (ieee_is_nan(pdfvals(8,m,l,j)[1])) pdfvals(8,m,l,j)[1] = 0._dp
                                if (ieee_is_nan(pdfvals(9,m,l,j)[1])) pdfvals(9,m,l,j)[1] = 0._dp
                                if (ieee_is_nan(pdfvals(10,m,l,j)[1])) pdfvals(10,m,l,j)[1] = 0._dp
                                if (ieee_is_nan(pdfvals(11,m,l,j)[1])) pdfvals(11,m,l,j)[1] = 0._dp

                                ! otherwise format length is too short for for E+XXX,
                                if (abs(pdfvals(1,m,l,j)[1]) < 1d-99) pdfvals(1,m,l,j)[1] = 0._dp
                                if (abs(pdfvals(2,m,l,j)[1]) < 1d-99) pdfvals(2,m,l,j)[1] = 0._dp
                                if (abs(pdfvals(3,m,l,j)[1]) < 1d-99) pdfvals(3,m,l,j)[1] = 0._dp
                                if (abs(pdfvals(4,m,l,j)[1]) < 1d-99) pdfvals(4,m,l,j)[1] = 0._dp
                                if (abs(pdfvals(5,m,l,j)[1]) < 1d-99) pdfvals(5,m,l,j)[1] = 0._dp
                                if (abs(pdfvals(6,m,l,j)[1]) < 1d-99) pdfvals(6,m,l,j)[1] = 0._dp
                                if (abs(pdfvals(7,m,l,j)[1]) < 1d-99) pdfvals(7,m,l,j)[1] = 0._dp
                                if (abs(pdfvals(8,m,l,j)[1]) < 1d-99) pdfvals(8,m,l,j)[1] = 0._dp
                                if (abs(pdfvals(9,m,l,j)[1]) < 1d-99) pdfvals(9,m,l,j)[1] = 0._dp
                                if (abs(pdfvals(10,m,l,j)[1]) < 1d-99) pdfvals(10,m,l,j)[1] = 0._dp
                                if (abs(pdfvals(11,m,l,j)[1]) < 1d-99) pdfvals(11,m,l,j)[1] = 0._dp

                            enddo
                        enddo
!$omp end parallel do
                    else
                        continue
                    endif
                enddo
                 write (*,*) "Image ", this_image(), " checked all work"
                sync all
            enddo
        endif

        if (this_image() /= 1) then
            return
        endif

#elif defined(HAVE_MPI)
        logical :: working(world_size-1)
        logical :: morework
        integer :: nextx, recvx, mpistatus(mpi_status_size)
        integer :: wxid
        real(dp) :: wx
        real(dp), allocatable :: pdfvals_slice(:,:,:)

        working(:) = .false.

        if (rank /= 0) then

            write (*,*) "Worker on rank ", rank, " launched"

            do while (.true.)

            call mpi_recv(wx, 1, mpi_double_precision, 0, 1, mpi_comm_world, mpistatus, ierr)
            call mpi_recv(wxid, 1, mpi_integer, 0, 2, mpi_comm_world, mpistatus, ierr)
            call mpi_recv(numq2, 1, mpi_integer, 0, 3, mpi_comm_world, mpistatus, ierr)
            call mpi_recv(numflavors, 1, mpi_integer, 0, 4, mpi_comm_world, mpistatus, ierr)
            call mpi_recv(q2vals, size(q2vals), mpi_double_precision, 0, 5, mpi_comm_world, mpistatus, ierr)
            call mpi_recv(flavors, size(flavors), mpi_integer, 0, 6, mpi_comm_world, mpistatus, ierr)
            call mpi_recv(curPDF, 1, mpi_integer, 0, 11, mpi_comm_world, mpistatus, ierr)


            if (allocated(pdfvals_slice)) deallocate(pdfvals_slice)
            allocate(pdfvals_slice(11,numflavors,numq2), source=huge(0._dp))

!$omp parallel do collapse(2)
            do l=1,numq2
                do m=1,numflavors
                    fl = flavors(m)
                    if (fl == 21) fl = 0
                    ! we hardcode ih=1 and beam=1
                    ! this means no pdfchannel selection should be done in the input file
                    ! for this grid generation
                    pdfvals_slice(1,m,l) = getbeam(1, fl, 0, 0, wx, q2vals(l))*wx
                    pdfvals_slice(2,m,l) = getbeam(1, fl, 1, 0, wx, q2vals(l))*wx
                    pdfvals_slice(3,m,l) = getbeam(1, fl, 1, 1, wx, q2vals(l))*wx
                    pdfvals_slice(4,m,l) = getbeam(1, fl, 2, 0, wx, q2vals(l))*wx
                    pdfvals_slice(5,m,l) = getbeam(1, fl, 2, 1, wx, q2vals(l))*wx
                    pdfvals_slice(6,m,l) = getbeam(1, fl, 2, 2, wx, q2vals(l))*wx

                    pdfvals_slice(7,m,l) = getbeam(1, fl, 3, 0, wx, q2vals(l))*wx
                    pdfvals_slice(8,m,l) = getbeam(1, fl, 3, 1, wx, q2vals(l))*wx
                    pdfvals_slice(9,m,l) = getbeam(1, fl, 3, 2, wx, q2vals(l))*wx
                    pdfvals_slice(10,m,l) = getbeam(1, fl, 3, 3, wx, q2vals(l))*wx

                    pdfvals_slice(11,m,l) = getbeam2(1, fl, 1, 0, wx, q2vals(l))*wx

                    if (ieee_is_nan(pdfvals_slice(1,m,l))) pdfvals_slice(1,m,l) = 0._dp
                    if (ieee_is_nan(pdfvals_slice(2,m,l))) pdfvals_slice(2,m,l) = 0._dp
                    if (ieee_is_nan(pdfvals_slice(3,m,l))) pdfvals_slice(3,m,l) = 0._dp
                    if (ieee_is_nan(pdfvals_slice(4,m,l))) pdfvals_slice(4,m,l) = 0._dp
                    if (ieee_is_nan(pdfvals_slice(5,m,l))) pdfvals_slice(5,m,l) = 0._dp
                    if (ieee_is_nan(pdfvals_slice(6,m,l))) pdfvals_slice(6,m,l) = 0._dp
                    if (ieee_is_nan(pdfvals_slice(7,m,l))) pdfvals_slice(7,m,l) = 0._dp
                    if (ieee_is_nan(pdfvals_slice(8,m,l))) pdfvals_slice(8,m,l) = 0._dp
                    if (ieee_is_nan(pdfvals_slice(9,m,l))) pdfvals_slice(9,m,l) = 0._dp
                    if (ieee_is_nan(pdfvals_slice(10,m,l))) pdfvals_slice(10,m,l) = 0._dp
                    if (ieee_is_nan(pdfvals_slice(11,m,l))) pdfvals_slice(11,m,l) = 0._dp

                    if (abs(pdfvals_slice(1,m,l)) < 1d-99) pdfvals_slice(1,m,l) = 0._dp
                    if (abs(pdfvals_slice(2,m,l)) < 1d-99) pdfvals_slice(2,m,l) = 0._dp
                    if (abs(pdfvals_slice(3,m,l)) < 1d-99) pdfvals_slice(3,m,l) = 0._dp
                    if (abs(pdfvals_slice(4,m,l)) < 1d-99) pdfvals_slice(4,m,l) = 0._dp
                    if (abs(pdfvals_slice(5,m,l)) < 1d-99) pdfvals_slice(5,m,l) = 0._dp
                    if (abs(pdfvals_slice(6,m,l)) < 1d-99) pdfvals_slice(6,m,l) = 0._dp
                    if (abs(pdfvals_slice(7,m,l)) < 1d-99) pdfvals_slice(7,m,l) = 0._dp
                    if (abs(pdfvals_slice(8,m,l)) < 1d-99) pdfvals_slice(8,m,l) = 0._dp
                    if (abs(pdfvals_slice(9,m,l)) < 1d-99) pdfvals_slice(9,m,l) = 0._dp
                    if (abs(pdfvals_slice(10,m,l)) < 1d-99) pdfvals_slice(10,m,l) = 0._dp
                    if (abs(pdfvals_slice(11,m,l)) < 1d-99) pdfvals_slice(11,m,l) = 0._dp
                enddo
            enddo
!$omp end parallel do

            ! signal master work is ready to pick up
            call mpi_send(wxid, 1, mpi_integer, 0, 1, mpi_comm_world, ierr)

            ! this is the signal to send work to master
            call mpi_recv(morework, 1, mpi_logical, 0, 0, mpi_comm_world, mpistatus, ierr)

            call mpi_send(pdfvals_slice, 11*numflavors*numq2, mpi_double_precision, &
                0, 2, mpi_comm_world, ierr)

            enddo
        endif

#endif

        call cfg_get(cfg, "resummation%gridoutpath", outdir)
        call cfg_get(cfg, "resummation%gridinpath", dir)

        pdfentry = 0
        do i=1,numPDFsets
            if (dopdferrors .eqv. .true.) then
                numpdfmembers = lhapdf_number(trim(PDFnames(i)))
            else
                numpdfmembers = 1
            endif

            write (*,*) "RUNNING ","mkdir -p "//trim(outdir)//trim(PDFnames(i))//'_B00'
            ! we repeat the writeout for all six necessary grids
            call execute_command_line("mkdir -p "//trim(outdir)//trim(PDFnames(i))//'_B00')
            call execute_command_line("cp "//trim(dir)//trim(PDFnames(i))//'/'//trim(PDFnames(i))//'.info ' &
                //trim(outdir)//trim(PDFnames(i))//'_B00/'//trim(PDFnames(i))//'_B00.info' )

            call execute_command_line("mkdir -p "//trim(outdir)//trim(PDFnames(i))//'_B10')
            call execute_command_line("cp "//trim(dir)//trim(PDFnames(i))//'/'//trim(PDFnames(i))//'.info ' &
                //trim(outdir)//trim(PDFnames(i))//'_B10/'//trim(PDFnames(i))//'_B10.info' )

            call execute_command_line("mkdir -p "//trim(outdir)//trim(PDFnames(i))//'_B11')
            call execute_command_line("cp "//trim(dir)//trim(PDFnames(i))//'/'//trim(PDFnames(i))//'.info ' &
                //trim(outdir)//trim(PDFnames(i))//'_B11/'//trim(PDFnames(i))//'_B11.info' )

            call execute_command_line("mkdir -p "//trim(outdir)//trim(PDFnames(i))//'_B20')
            call execute_command_line("cp "//trim(dir)//trim(PDFnames(i))//'/'//trim(PDFnames(i))//'.info ' &
                //trim(outdir)//trim(PDFnames(i))//'_B20/'//trim(PDFnames(i))//'_B20.info' )

            call execute_command_line("mkdir -p "//trim(outdir)//trim(PDFnames(i))//'_B21')
            call execute_command_line("cp "//trim(dir)//trim(PDFnames(i))//'/'//trim(PDFnames(i))//'.info ' &
                //trim(outdir)//trim(PDFnames(i))//'_B21/'//trim(PDFnames(i))//'_B21.info' )

            call execute_command_line("mkdir -p "//trim(outdir)//trim(PDFnames(i))//'_B22')
            call execute_command_line("cp "//trim(dir)//trim(PDFnames(i))//'/'//trim(PDFnames(i))//'.info ' &
                //trim(outdir)//trim(PDFnames(i))//'_B22/'//trim(PDFnames(i))//'_B22.info' )

            call execute_command_line("mkdir -p "//trim(outdir)//trim(PDFnames(i))//'_B30')
            call execute_command_line("cp "//trim(dir)//trim(PDFnames(i))//'/'//trim(PDFnames(i))//'.info ' &
                //trim(outdir)//trim(PDFnames(i))//'_B30/'//trim(PDFnames(i))//'_B30.info' )

            call execute_command_line("mkdir -p "//trim(outdir)//trim(PDFnames(i))//'_B31')
            call execute_command_line("cp "//trim(dir)//trim(PDFnames(i))//'/'//trim(PDFnames(i))//'.info ' &
                //trim(outdir)//trim(PDFnames(i))//'_B31/'//trim(PDFnames(i))//'_B31.info' )

            call execute_command_line("mkdir -p "//trim(outdir)//trim(PDFnames(i))//'_B32')
            call execute_command_line("cp "//trim(dir)//trim(PDFnames(i))//'/'//trim(PDFnames(i))//'.info ' &
                //trim(outdir)//trim(PDFnames(i))//'_B32/'//trim(PDFnames(i))//'_B32.info' )

            call execute_command_line("mkdir -p "//trim(outdir)//trim(PDFnames(i))//'_B33')
            call execute_command_line("cp "//trim(dir)//trim(PDFnames(i))//'/'//trim(PDFnames(i))//'.info ' &
                //trim(outdir)//trim(PDFnames(i))//'_B33/'//trim(PDFnames(i))//'_B33.info' )

            ! for gg
            call execute_command_line("mkdir -p "//trim(outdir)//trim(PDFnames(i))//'_G10')
            call execute_command_line("cp "//trim(dir)//trim(PDFnames(i))//'/'//trim(PDFnames(i))//'.info ' &
                //trim(outdir)//trim(PDFnames(i))//'_G10/'//trim(PDFnames(i))//'_G10.info' )


            do j=0,numpdfmembers-1
                write (*,*) "PROCESSING ", j+1, " of ", numpdfmembers, " pdf members"
                currentpdf = pdfentry

                write (outpath, '(A,I4.4,A)') trim(outdir)//trim(PDFnames(i))//'_B00/'// &
                    trim(PDFnames(i))//'_B00_', j, '.dat'
                open(unit=15, file=outpath, status='replace', form='formatted')

                write (outpath, '(A,I4.4,A)') trim(outdir)//trim(PDFnames(i))//'_B10/'// &
                    trim(PDFnames(i))//'_B10_', j, '.dat'
                open(unit=16, file=outpath, status='replace', form='formatted')

                write (outpath, '(A,I4.4,A)') trim(outdir)//trim(PDFnames(i))//'_B11/'// &
                    trim(PDFnames(i))//'_B11_', j, '.dat'
                open(unit=17, file=outpath, status='replace', form='formatted')

                write (outpath, '(A,I4.4,A)') trim(outdir)//trim(PDFnames(i))//'_B20/'// &
                    trim(PDFnames(i))//'_B20_', j, '.dat'
                open(unit=18, file=outpath, status='replace', form='formatted')

                write (outpath, '(A,I4.4,A)') trim(outdir)//trim(PDFnames(i))//'_B21/'// &
                    trim(PDFnames(i))//'_B21_', j, '.dat'
                open(unit=19, file=outpath, status='replace', form='formatted')

                write (outpath, '(A,I4.4,A)') trim(outdir)//trim(PDFnames(i))//'_B22/'// &
                    trim(PDFnames(i))//'_B22_', j, '.dat'
                open(unit=20, file=outpath, status='replace', form='formatted')

                write (outpath, '(A,I4.4,A)') trim(outdir)//trim(PDFnames(i))//'_B30/'// &
                    trim(PDFnames(i))//'_B30_', j, '.dat'
                open(unit=21, file=outpath, status='replace', form='formatted')

                write (outpath, '(A,I4.4,A)') trim(outdir)//trim(PDFnames(i))//'_B31/'// &
                    trim(PDFnames(i))//'_B31_', j, '.dat'
                open(unit=22, file=outpath, status='replace', form='formatted')

                write (outpath, '(A,I4.4,A)') trim(outdir)//trim(PDFnames(i))//'_B32/'// &
                    trim(PDFnames(i))//'_B32_', j, '.dat'
                open(unit=23, file=outpath, status='replace', form='formatted')

                write (outpath, '(A,I4.4,A)') trim(outdir)//trim(PDFnames(i))//'_B33/'// &
                    trim(PDFnames(i))//'_B33_', j, '.dat'
                open(unit=24, file=outpath, status='replace', form='formatted')

                write (outpath, '(A,I4.4,A)') trim(outdir)//trim(PDFnames(i))//'_G10/'// &
                    trim(PDFnames(i))//'_G10_', j, '.dat'
                open(unit=25, file=outpath, status='replace', form='formatted')

            

                write (*,'(A,A,A,I3)') "Processing ", trim(PDFnames(i)), " member ", j
                write (path, '(A,I4.4,A)') trim(dir)//trim(PDFnames(i))//'/'//trim(PDFnames(i))//'_', j, '.dat'
                open(unit=11, file=trim(path), status='old', form='formatted', iostat=ierr, iomsg=imsg)

!               read(11,'(A)') pdftype
!               write(unit=15,fmt='(A)') trim(pdftype)
!               write(unit=16,fmt='(A)') trim(pdftype)
!               write(unit=17,fmt='(A)') trim(pdftype)
!               write(unit=18,fmt='(A)') trim(pdftype)
!               write(unit=19,fmt='(A)') trim(pdftype)
!               write(unit=20,fmt='(A)') trim(pdftype)
!               write(unit=21,fmt='(A)') trim(pdftype)

!               read(11,'(A)') pdfformat
!               write(unit=15,fmt='(A)') trim(pdfformat)
!               write(unit=16,fmt='(A)') trim(pdfformat)
!               write(unit=17,fmt='(A)') trim(pdfformat)
!               write(unit=18,fmt='(A)') trim(pdfformat)
!               write(unit=19,fmt='(A)') trim(pdfformat)
!               write(unit=20,fmt='(A)') trim(pdfformat)
!               write(unit=21,fmt='(A)') trim(pdfformat)

!               read(11,'(A)') dummy
!               write(unit=15,fmt='(A)') trim(dummy)
!               write(unit=16,fmt='(A)') trim(dummy)
!               write(unit=17,fmt='(A)') trim(dummy)
!               write(unit=18,fmt='(A)') trim(dummy)
!               write(unit=19,fmt='(A)') trim(dummy)
!               write(unit=20,fmt='(A)') trim(dummy)
!               write(unit=21,fmt='(A)') trim(dummy)

                ierr = 0
                do while (ierr == 0)
                    read(11, '(A)') nextline
                    write(unit=15,fmt='(A)') trim(nextline)
                    write(unit=16,fmt='(A)') trim(nextline)
                    write(unit=17,fmt='(A)') trim(nextline)
                    write(unit=18,fmt='(A)') trim(nextline)
                    write(unit=19,fmt='(A)') trim(nextline)
                    write(unit=20,fmt='(A)') trim(nextline)
                    write(unit=21,fmt='(A)') trim(nextline)
                    write(unit=22,fmt='(A)') trim(nextline)
                    write(unit=23,fmt='(A)') trim(nextline)
                    write(unit=24,fmt='(A)') trim(nextline)
                    write(unit=25,fmt='(A)') trim(nextline)
                    if (trim(nextline) == "---") then
                        ierr = 1
                    endif
                enddo

                ierr = 0
                read(11,'(A)', iostat=ierr) nextline
                do while (ierr == 0)

                    xvals = huge(0._dp)
                    read(nextline, *, iostat=iread) xvals

                    read(11,'(A)') nextline
                    q2vals = huge(0._dp)
                    read(nextline, *, iostat=iread) q2vals

                    read(11,'(A)') nextline
                    flavors = huge(0)
                    read(nextline, *, iostat=iread) flavors

                    numx = count(xvals /= huge(0._dp))
                    numq2 = count(q2vals /= huge(0._dp))
                    numflavors = count(flavors /= huge(0))

                    write (*,'(A,I3,A,I3,A,I3)') "Processing block with number of x,q2,flavors ", numx, " ", numq2, " ", numflavors

                    do k=1,numx
                        write(unit=15, fmt='(ES16.8)', advance="no") xvals(k)
                        write(unit=16, fmt='(ES16.8)', advance="no") xvals(k)
                        write(unit=17, fmt='(ES16.8)', advance="no") xvals(k)
                        write(unit=18, fmt='(ES16.8)', advance="no") xvals(k)
                        write(unit=19, fmt='(ES16.8)', advance="no") xvals(k)
                        write(unit=20, fmt='(ES16.8)', advance="no") xvals(k)
                        write(unit=21, fmt='(ES16.8)', advance="no") xvals(k)
                        write(unit=22, fmt='(ES16.8)', advance="no") xvals(k)
                        write(unit=23, fmt='(ES16.8)', advance="no") xvals(k)
                        write(unit=24, fmt='(ES16.8)', advance="no") xvals(k)
                        write(unit=25, fmt='(ES16.8)', advance="no") xvals(k)
                    enddo
                    write (unit=15, fmt='(A)') ''
                    write (unit=16, fmt='(A)') ''
                    write (unit=17, fmt='(A)') ''
                    write (unit=18, fmt='(A)') ''
                    write (unit=19, fmt='(A)') ''
                    write (unit=20, fmt='(A)') ''
                    write (unit=21, fmt='(A)') ''
                    write (unit=22, fmt='(A)') ''
                    write (unit=23, fmt='(A)') ''
                    write (unit=24, fmt='(A)') ''
                    write (unit=25, fmt='(A)') ''

                    do k=1,numq2
                        write(unit=15, fmt='(ES16.8)', advance="no") q2vals(k)
                        write(unit=16, fmt='(ES16.8)', advance="no") q2vals(k)
                        write(unit=17, fmt='(ES16.8)', advance="no") q2vals(k)
                        write(unit=18, fmt='(ES16.8)', advance="no") q2vals(k)
                        write(unit=19, fmt='(ES16.8)', advance="no") q2vals(k)
                        write(unit=20, fmt='(ES16.8)', advance="no") q2vals(k)
                        write(unit=21, fmt='(ES16.8)', advance="no") q2vals(k)
                        write(unit=22, fmt='(ES16.8)', advance="no") q2vals(k)
                        write(unit=23, fmt='(ES16.8)', advance="no") q2vals(k)
                        write(unit=24, fmt='(ES16.8)', advance="no") q2vals(k)
                        write(unit=25, fmt='(ES16.8)', advance="no") q2vals(k)
                    enddo
                    write (unit=15, fmt='(A)') ''
                    write (unit=16, fmt='(A)') ''
                    write (unit=17, fmt='(A)') ''
                    write (unit=18, fmt='(A)') ''
                    write (unit=19, fmt='(A)') ''
                    write (unit=20, fmt='(A)') ''
                    write (unit=21, fmt='(A)') ''
                    write (unit=22, fmt='(A)') ''
                    write (unit=23, fmt='(A)') ''
                    write (unit=24, fmt='(A)') ''
                    write (unit=25, fmt='(A)') ''

                    do k=1,numflavors
                        write(unit=15, fmt='(I3)', advance="no") flavors(k)
                        write(unit=16, fmt='(I3)', advance="no") flavors(k)
                        write(unit=17, fmt='(I3)', advance="no") flavors(k)
                        write(unit=18, fmt='(I3)', advance="no") flavors(k)
                        write(unit=19, fmt='(I3)', advance="no") flavors(k)
                        write(unit=20, fmt='(I3)', advance="no") flavors(k)
                        write(unit=21, fmt='(I3)', advance="no") flavors(k)
                        write(unit=22, fmt='(I3)', advance="no") flavors(k)
                        write(unit=23, fmt='(I3)', advance="no") flavors(k)
                        write(unit=24, fmt='(I3)', advance="no") flavors(k)
                        write(unit=25, fmt='(I3)', advance="no") flavors(k)
                    enddo


                    ! m: flavors
                    ! l: q2
                    ! k: x
                    ! beam 1:6, m, l, k
#ifndef HAVE_COARRAY
                    if (allocated(pdfvals)) deallocate(pdfvals)
                    allocate(pdfvals(11,numflavors,numq2,numx), source=huge(0._dp))
#endif

! with MPI we distribute x values to workers and fill pdfvals
#ifdef HAVE_MPI
                    if (allocated(pdfvals_slice)) deallocate(pdfvals_slice)
                    allocate(pdfvals_slice(11,numflavors,numq2), source=huge(0._dp))

                    working(:) = .false.

                    nextx = 1
                    recvx = 0
                    do while (recvx < numx)
                        do n=1,world_size-1
                            if ((.not. working(n)) .and. (nextx <= numx)) then
                                wx = xvals(nextx)
                                wxid = nextx
                                nextx = nextx + 1

                                ! push wx, wxid and other stuff to worker
                                call mpi_send(wx, 1, mpi_double_precision, n, 1, mpi_comm_world, ierr)
                                call mpi_send(wxid, 1, mpi_integer, n, 2, mpi_comm_world, ierr)
                                call mpi_send(numq2, 1, mpi_integer, n, 3, mpi_comm_world, ierr)
                                call mpi_send(numflavors, 1, mpi_integer, n, 4, mpi_comm_world, ierr)
                                call mpi_send(q2vals, size(q2vals), mpi_double_precision, n, 5, mpi_comm_world, ierr)
                                call mpi_send(flavors, size(flavors), mpi_integer, n, 6, mpi_comm_world, ierr)
                                call mpi_send(currentPDF, 1, mpi_integer, n, 11, mpi_comm_world, ierr)

                                ! skip these in the input file
                                do l=1,numq2
                                    read(11,'(A)', iostat=iread) nextline
                                enddo

                                working(n) = .true.
                            endif
                        enddo

                        ! receive one finished task
                        mpistatus = 0
                        call mpi_recv(wxid, 1, mpi_integer, mpi_any_source, 1, mpi_comm_world, mpistatus, ierr)

                        !if ((j < numpdfmembers-1) .and. (i < numPDFsets) .and. (recvx+1 < numx)) then
                            morework = .true.
                        !else
                            !morework = .false.
                        !endif

                        ! signal worker that it should send stuff
                        call mpi_send(morework, 1, mpi_logical, mpistatus(mpi_source), 0, mpi_comm_world, ierr)

                        write (*,'(A,ES16.8,A,I3)') "MASTER: Receiving x = ", xvals(wxid), " from rank ", mpistatus(mpi_source)
                        call mpi_recv(pdfvals_slice, 11*numflavors*numq2, &
                            mpi_double_precision, mpistatus(mpi_source), 2, mpi_comm_world, mpistatus, ierr)
                        pdfvals(:,:,:,wxid) = pdfvals_slice(:,:,:)
                        working(mpistatus(mpi_source)) = .false.
                        recvx = recvx + 1
                    enddo

#elif HAVE_COARRAY
                    do nimg=2,num_images()
                        numx[nimg] = numx
                        numq2[nimg] = numq2
                        numflavors[nimg] = numflavors
                        flavors(1:numflavors)[nimg] = flavors(1:numflavors)
                        q2vals(1:numq2)[nimg] = q2vals(1:numq2)
                        xvals(1:numx)[nimg] = xvals(1:numx)
                        curPDF[nimg] = currentPDF
                    enddo
                    sync all
                    if (allocated(pdfvals)) deallocate(pdfvals)
                    allocate(pdfvals(11,numflavors,numq2,numx)[*], source=huge(0._dp))
                    if (allocated(work_done)) deallocate(work_done)
                    allocate(work_done(numx)[*], source=.false.)
                    sync all

                    do k=1,numx
                        do l=1,numq2
                            read(11,'(A)', iostat=iread) nextline
                        enddo
                    enddo

! without MPI or Coarray we calculate everything local
#else
                    do k=1,numx
                        do l=1,numq2
                            read(11,'(A)', iostat=iread) nextline
                        enddo
                    enddo

                    do k=1,numx
                        write (*,'(A,ES16.8)') "Processing x = ", xvals(k)
!$omp parallel do collapse(2)
                        do l=1,numq2

                            ! original grid values
                            !read(nextline, *, iostat=iread) pdfvals(:,l,1)

                            ! compute beam for current x and q2
                            do m=1,numflavors
                                fl = flavors(m)
                                if (fl == 21) fl = 0

                                ! we hardcode ih=1 and beam=1
                                ! this means no pdfchannel selection should be done in the input file
                                ! for this grid generation
                                pdfvals(1,m,l,k) = getbeam(1, fl, 0, 0, xvals(k), q2vals(l))*xvals(k)
                                pdfvals(2,m,l,k) = getbeam(1, fl, 1, 0, xvals(k), q2vals(l))*xvals(k)
                                pdfvals(3,m,l,k) = getbeam(1, fl, 1, 1, xvals(k), q2vals(l))*xvals(k)
                                pdfvals(4,m,l,k) = getbeam(1, fl, 2, 0, xvals(k), q2vals(l))*xvals(k)
                                pdfvals(5,m,l,k) = getbeam(1, fl, 2, 1, xvals(k), q2vals(l))*xvals(k)
                                pdfvals(6,m,l,k) = getbeam(1, fl, 2, 2, xvals(k), q2vals(l))*xvals(k)

                                pdfvals(7,m,l,k) = getbeam(1, fl, 3, 0, xvals(k), q2vals(l))*xvals(k)
                                pdfvals(8,m,l,k) = getbeam(1, fl, 3, 1, xvals(k), q2vals(l))*xvals(k)
                                pdfvals(9,m,l,k) = getbeam(1, fl, 3, 2, xvals(k), q2vals(l))*xvals(k)
                                pdfvals(10,m,l,k) = getbeam(1, fl, 3, 3, xvals(k), q2vals(l))*xvals(k)

                                pdfvals(11,m,l,k) = getbeam2(1, fl, 1, 0, xvals(k), q2vals(l))*xvals(k)

                                if (ieee_is_nan(pdfvals(1,m,l,k))) pdfvals(1,m,l,k) = 0._dp
                                if (ieee_is_nan(pdfvals(2,m,l,k))) pdfvals(2,m,l,k) = 0._dp
                                if (ieee_is_nan(pdfvals(3,m,l,k))) pdfvals(3,m,l,k) = 0._dp
                                if (ieee_is_nan(pdfvals(4,m,l,k))) pdfvals(4,m,l,k) = 0._dp
                                if (ieee_is_nan(pdfvals(5,m,l,k))) pdfvals(5,m,l,k) = 0._dp
                                if (ieee_is_nan(pdfvals(6,m,l,k))) pdfvals(6,m,l,k) = 0._dp
                                if (ieee_is_nan(pdfvals(7,m,l,k))) pdfvals(7,m,l,k) = 0._dp
                                if (ieee_is_nan(pdfvals(8,m,l,k))) pdfvals(8,m,l,k) = 0._dp
                                if (ieee_is_nan(pdfvals(9,m,l,k))) pdfvals(9,m,l,k) = 0._dp
                                if (ieee_is_nan(pdfvals(10,m,l,k))) pdfvals(10,m,l,k) = 0._dp
                                if (ieee_is_nan(pdfvals(11,m,l,k))) pdfvals(11,m,l,k) = 0._dp
                            enddo
                        enddo
!$omp end parallel do
                    enddo
#endif

                    do k=1,numx
                        do l=1,numq2
                            write(unit=15, fmt='(A)') ''
                            write(unit=16, fmt='(A)') ''
                            write(unit=17, fmt='(A)') ''
                            write(unit=18, fmt='(A)') ''
                            write(unit=19, fmt='(A)') ''
                            write(unit=20, fmt='(A)') ''
                            write(unit=21, fmt='(A)') ''
                            write(unit=22, fmt='(A)') ''
                            write(unit=23, fmt='(A)') ''
                            write(unit=24, fmt='(A)') ''
                            write(unit=25, fmt='(A)') ''
                            do m=1,numflavors
                                write (unit=15, fmt='(ES16.8)', advance="no") pdfvals(1,m,l,k)
                                write (unit=16, fmt='(ES16.8)', advance="no") pdfvals(2,m,l,k)
                                write (unit=17, fmt='(ES16.8)', advance="no") pdfvals(3,m,l,k)
                                write (unit=18, fmt='(ES16.8)', advance="no") pdfvals(4,m,l,k)
                                write (unit=19, fmt='(ES16.8)', advance="no") pdfvals(5,m,l,k)
                                write (unit=20, fmt='(ES16.8)', advance="no") pdfvals(6,m,l,k)
                                write (unit=21, fmt='(ES16.8)', advance="no") pdfvals(7,m,l,k)
                                write (unit=22, fmt='(ES16.8)', advance="no") pdfvals(8,m,l,k)
                                write (unit=23, fmt='(ES16.8)', advance="no") pdfvals(9,m,l,k)
                                write (unit=24, fmt='(ES16.8)', advance="no") pdfvals(10,m,l,k)
                                write (unit=25, fmt='(ES16.8)', advance="no") pdfvals(11,m,l,k)
                            enddo
                        enddo
                    enddo

                    read(11,'(A)', iostat=ierr) dummy
                    write (unit=15, fmt='(A)') ''
                    write (unit=15, fmt='(A)') '---'

                    write (unit=16, fmt='(A)') ''
                    write (unit=16, fmt='(A)') '---'

                    write (unit=17, fmt='(A)') ''
                    write (unit=17, fmt='(A)') '---'

                    write (unit=18, fmt='(A)') ''
                    write (unit=18, fmt='(A)') '---'

                    write (unit=19, fmt='(A)') ''
                    write (unit=19, fmt='(A)') '---'

                    write (unit=20, fmt='(A)') ''
                    write (unit=20, fmt='(A)') '---'

                    write (unit=21, fmt='(A)') ''
                    write (unit=21, fmt='(A)') '---'

                    write (unit=22, fmt='(A)') ''
                    write (unit=22, fmt='(A)') '---'

                    write (unit=23, fmt='(A)') ''
                    write (unit=23, fmt='(A)') '---'

                    write (unit=24, fmt='(A)') ''
                    write (unit=24, fmt='(A)') '---'

                    write (unit=25, fmt='(A)') ''
                    write (unit=25, fmt='(A)') '---'

                    read(11,'(A)', iostat=ierr) nextline
                end do

                close(unit=11)

                close(unit=15)
                close(unit=16)
                close(unit=17)
                close(unit=18)
                close(unit=19)
                close(unit=20)
                close(unit=21)
                close(unit=22)
                close(unit=23)
                close(unit=24)
                close(unit=25)

                pdfentry = pdfentry + 1

            enddo
        enddo

        currentpdf = 0

        write (*,*) "Grid generation is complete."
        write (*,*) "Please copy the generated PDF sets to your $LHAPDF_DATA_PATH"
        write (*,*) "Then set makegrid to .false."
        write (*,*) ""
        write (*,*) "Warning: Please remove any ForcePositive: 1 settings"
        write (*,*) "in the LHAPDF .info files. Most prominently this affects"
        write (*,*) "CT14nnlo."
        stop 0

    end subroutine


end module

