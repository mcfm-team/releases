!
!  SPDX-License-Identifier: GPL-3.0-or-later
!  Copyright (C) 2019-2022, respective authors of MCFM.
!
 
select case (powAs)
    case (0)
        select case (powLperp)
            case (0)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (1)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (2)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (3)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case default
                Ibar_select = 0._dp
        end select
    case (1)
        select case (powLperp)
            case (0)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (1)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (2)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (3)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case default
                Ibar_select = 0._dp
        end select
    case (2)
        select case (powLperp)
            case (0)
                select case (type_in)
                    case (1)
                        Ibar_select = z**3*(-3.6040303008016474_dp) + (-3.2270183101427925_dp) + z**5*(-0.0013164390324173112_dp) + &
                        z**4*(0.05637832379670145_dp) + z*(0.5603571563984158_dp) + (2.6451727005890224_dp)/z + &
                        z**2*(3.5704568978264084_dp) + (z*(-5.333333333333333_dp) + z**3*(1.215623396613648_dp) + &
                        (2.6666666666666665_dp) + z**2*(5.654771516227999_dp))*Log(z) + (z**2*(-1.773058773058773_dp) + &
                        (-0.6666666666666666_dp) + z*(-0.6666666666666666_dp) + z**3*(-0.14346862340441793_dp))*Log(z)**2 + &
                        (z**3*(-0.0170296134428453_dp) + z**2*(0.00030671710459053266_dp) + (0.4444444444444444_dp) + &
                        z*(0.4444444444444444_dp))*Log(z)**3

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (1)
                select case (type_in)
                    case (1)
                        Ibar_select = z*(-16._dp) + (-7.703703703703703_dp)/z + (10.666666666666666_dp) + &
                        z**2*(13.037037037037036_dp) + (z*(-8._dp) + z**2*(-7.111111111111111_dp) + (-2.6666666666666665_dp))*Log(z) &
                        + ((2.6666666666666665_dp) + z*(2.6666666666666665_dp))*Log(z)**2

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (2)
                select case (type_in)
                    case (1)
                        Ibar_select = z**2*(-1.7777777777777777_dp) + z*(-1.3333333333333333_dp) + (1.3333333333333333_dp) + &
                        (1.7777777777777777_dp)/z + ((2.6666666666666665_dp) + z*(2.6666666666666665_dp))*Log(z)

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (3)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case default
                Ibar_select = 0._dp
        end select
    case (3)
        select case (powLperp)
            case (0)
                select case (type_in)
                    case (1)
                        Ibar_select = (z**4*(-3.3428717235281533e8_dp) + z**5*(-3.24224155366712e6_dp) + (-472.18580115215207_dp) + &
                        Nf*(z*(-59.09868138406361_dp) + z**3*(-24.98539332433859_dp) + z**5*(-0.9090821349290608_dp) + &
                        z**6*(0.10213731794968792_dp) + (5.88282387997262_dp) + z**4*(18.534746870913505_dp) + &
                        z**2*(60.47344877255524_dp)) + z*(383.21247167343734_dp) + z**2*(3482.133682638759_dp) + &
                        z**6*(39332.022151092766_dp) + z**3*(3.3748668872374535e8_dp))/z + ((-532.9212801317947_dp) + &
                        z*(-261.0442275904388_dp) + (-78.98467140066984_dp)/z + Nf*((-30.966480714928217_dp) + &
                        z*(-12.003517751965253_dp) + z**3*(-10.540989729225023_dp) + z**2*(5.1478051820853_dp)) + &
                        z**2*(1.3739427845878354e8_dp) + z**3*(2.032629740752571e8_dp))*Log(z) + (z**3*(-5.370201460019887e7_dp) + &
                        z*(-85.93053325559873_dp) + Nf*((-10.549308734586743_dp) + z*(-8.771530956808965_dp) + &
                        z**2*(0.1997362426567558_dp) + z**3*(3.5376079174563064_dp)) + (86.37456229261306_dp) + &
                        z**2*(2.3899943818252087e7_dp))*Log(z)**2 + ((-47.9778548422999_dp) + Nf*((-1.6131687242798354_dp) + &
                        z*(-1.6131687242798354_dp) + z**3*(-0.3148828372387587_dp) + z**2*(0.7031832520791511_dp)) + &
                        z*(10.836959972514915_dp) + z**2*(2.2252031098020575e6_dp) + z**3*(9.71625446674031e6_dp))*Log(z)**3 + &
                        (z**3*(-887690.0078720667_dp) + Nf*((-0.24691358024691357_dp) + z*(-0.24691358024691357_dp) + &
                        z**2*(-0.002601203056413591_dp) + z**3*(0.11712315648343229_dp)) + z*(2.345679012345679_dp) + &
                        (7.160493827160494_dp) + z**2*(111169.26682149382_dp))*Log(z)**4 + ((-0.7407407407407407_dp) + &
                        z*(1.2740740740740741_dp) + z**2*(2394.735764969416_dp) + z**3*(85143.19508330338_dp))*Log(z)**5 + &
                        (z*(-7597.048542022794_dp) + Nf*(z**2*(-7.70122877162304_dp) + (-5.544759836563321_dp) + &
                        z**3*(3.185230352303523_dp) + z*(10.060758255882838_dp)) + z**3*(207.42737736297693_dp) + &
                        z**2*(3467.7479234389207_dp) + (3921.8732412208965_dp))*Log(z*(-1._dp) + (1._dp)) + &
                        (z**2*(-13570.930669636624_dp) + (-4224.945552988793_dp) + Nf*(z*(-1.1444946128399929_dp) + &
                        z**3*(0.030580661136560605_dp) + (0.3406240567413631_dp) + z**2*(0.773289894962069_dp)) + &
                        z**3*(4670.030672284503_dp) + z*(13125.845550340911_dp))*Log(z*(-1._dp) + (1._dp))**2 + &
                        (z*(-42.897419068067926_dp) + z**3*(-13.159651273668437_dp) + Nf*(z**2*(-0.6827091430256285_dp) + &
                        (-0.32694334958049026_dp) + z**3*(0.22726561277195179_dp) + z*(0.7823868798341669_dp)) + &
                        (15.140488835471352_dp) + z**2*(40.91658150626502_dp))*Log(z*(-1._dp) + (1._dp))**3 + &
                        (z**2*(-7.504455857694885_dp) + (-2.2065175424656394_dp) + z**3*(2.525512367491166_dp) + &
                        z*(7.185461032669358_dp))*Log(z*(-1._dp) + (1._dp))**4

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (1)
                select case (type_in)
                    case (1)
                        Ibar_select = (z**4*(-4.249162927705976e6_dp) + z**6*(-3102.8649511904587_dp) + z*(-786.5752835568283_dp) + &
                        Nf*(z*(-47.84250324323074_dp) + z**4*(-4.589616073204694_dp) + (-1.1565265637483262_dp) + &
                        z**5*(-1.0078269216569447_dp) + z**3*(0.17401837928153718_dp) + z**6*(0.2108997733320193_dp) + &
                        z**2*(54.211554625724375_dp)) + (483.9673467888446_dp) + z**2*(913.6618071555971_dp) + &
                        z**5*(136327.01682936714_dp) + z**3*(4.11532772220178e6_dp))/z + (z*(-266.40149302711694_dp) + &
                        Nf*((-29.870148425982446_dp) + z*(-16.833111388945408_dp) + z**3*(4.0626629422718805_dp) + &
                        z**2*(12.756509161041466_dp)) + (98.11244277743864_dp)/z + (271.58554555369165_dp) + &
                        z**2*(1.6464752327762763e6_dp) + z**3*(2.3399234807892493e6_dp))*Log(z) + (z**3*(-741668.8593692747_dp) + &
                        (-165.22408794784172_dp) + Nf*((-7.703703703703703_dp) + z*(-7.703703703703703_dp) + &
                        z**3*(-0.415594042725251_dp) + z**2*(2.7071215217733062_dp)) + z*(51.806554305301205_dp) + &
                        z**2*(267405.6199700369_dp))*Log(z)**2 + (z*(-5.62962962962963_dp) + Nf*((-1.1851851851851851_dp) + &
                        z*(-1.1851851851851851_dp) + z**2*(0.0169144727867958_dp) + z**3*(0.3615537167475894_dp)) + &
                        (34.074074074074076_dp) + z**2*(21001.958378529333_dp) + z**3*(90745.74570795924_dp))*Log(z)**3 + &
                        (z**3*(-14373.97085786397_dp) + (-4.444444444444445_dp) + z*(8.296296296296296_dp) + &
                        z**2*(679.0637456264286_dp))*Log(z)**4 + (z*(-772.4260346432461_dp) + z**3*(-263.58776869113706_dp) + &
                        Nf*(z*(-4.559282020884582_dp) + z**3*(-0.17257441490570324_dp) + (0.7118723215079577_dp) + &
                        z**2*(4.019984114282327_dp)) + (266.4126714347489_dp) + z**2*(769.6011318996342_dp))*Log(z*(-1._dp) + &
                        (1._dp)) + (z*(-74.74156853609831_dp) + z**3*(-21.393301000303122_dp) + Nf*(z**2*(-3.202431556813283_dp) + &
                        (-1.9599822537344245_dp) + z**3*(1.0656690959838735_dp) + z*(4.096744714563834_dp)) + &
                        (31.340800434564265_dp) + z**2*(64.79406910183718_dp))*Log(z*(-1._dp) + (1._dp))**2 + &
                        (z**2*(-10.102286050707095_dp) + (-1.9908893803281575_dp) + z**3*(3.3149575944487277_dp) + &
                        z*(8.778217836586524_dp))*Log(z*(-1._dp) + (1._dp))**3

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (2)
                select case (type_in)
                    case (1)
                        Ibar_select = (z**3*(-1879.0460834615833_dp) + (-220.95532436198914_dp) + z**2*(-158.81336518685896_dp) + &
                        z**6*(-20.021939275220372_dp) + Nf*(z*(-15.922728604826144_dp) + z**5*(-0.7777828865694975_dp) + &
                        z**4*(-0.47407834101382496_dp) + z**6*(0.1798342541436464_dp) + (1.1851851851851851_dp) + &
                        z**3*(5.623854999025531_dp) + z**2*(10.185715290166272_dp)) + z*(263.26740393874053_dp) + &
                        z**5*(307.82203849281643_dp) + z**4*(1707.7472716829411_dp))/z + (z**3*(-1482.192546009433_dp) + &
                        z**2*(-807.3621590952911_dp) + (-145.55928700679456_dp) + z*(-127.39461783961922_dp) + &
                        (-46.222222222222236_dp)/z + Nf*((-7.703703703703703_dp) + z*(-7.703703703703703_dp) + &
                        z**3*(-0.15299579924828655_dp) + z**2*(1.909043965771614_dp)))*Log(z) + (z**2*(-127.36102354904506_dp) + &
                        z*(-14.222222222222221_dp) + Nf*((-1.7777777777777777_dp) + z*(-1.7777777777777777_dp) + &
                        z**2*(-0.04630079326533915_dp) + z**3*(0.5050636719141682_dp)) + (38.222222222222214_dp) + &
                        z**3*(214.53622606133652_dp))*Log(z)**2 + (z**3*(-84.36428149024246_dp) + (-5.925925925925927_dp) + &
                        z**2*(-5.406404346394467_dp) + z*(15.407407407407407_dp))*Log(z)**3 + (z*(-177.77963268425535_dp) + &
                        z**3*(-30.466535499847353_dp) + Nf*(z**2*(-3.511929408639212_dp) + (-2.929127721470389_dp) + &
                        z**3*(1.1802897324733002_dp) + z*(5.260767397636301_dp)) + (87.4343263699818_dp) + &
                        z**2*(120.81184181412094_dp))*Log(z*(-1._dp) + (1._dp))

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (3)
                select case (type_in)
                    case (1)
                        Ibar_select = (-50.62385710459351_dp) + z**4*(-4.407436322826193_dp) + z**3*(-2.6864439571778664_dp) + &
                        z**2*(-0.5265500760136049_dp) + Nf*((-0.7901234567901234_dp)/z + (-0.5925925925925926_dp) + &
                        z*(0.5925925925925926_dp) + z**2*(0.7901234567901234_dp)) + z**5*(1.019060773480663_dp) + &
                        z*(18.114115066319957_dp) + (39.11111111111111_dp)/z + (z**2*(-8.935170641153789_dp) + &
                        Nf*((-1.1851851851851851_dp) + z*(-1.1851851851851851_dp)) + z**3*(-0.8669762220634759_dp) + &
                        (7.111111111111111_dp)/z + z*(9.481481481481485_dp) + (19.555555555555557_dp))*Log(z) + &
                        ((-2.9629629629629632_dp) + z**2*(-0.2623712088231026_dp) + z**3*(2.862027514358221_dp) + &
                        z*(13.037037037037036_dp))*Log(z)**2 + (z**2*(-19.900933497371888_dp) + (-16.598390495119958_dp) + &
                        z**3*(6.688308538163001_dp) + z*(29.811015454328846_dp))*Log(z*(-1._dp) + (1._dp))

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case default
                Ibar_select = 0._dp
        end select
    case default
        Ibar_select = 0._dp
end select
