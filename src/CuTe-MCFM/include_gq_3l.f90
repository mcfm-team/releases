!
!  SPDX-License-Identifier: GPL-3.0-or-later
!  Copyright (C) 2019-2022, respective authors of MCFM.
!
 
select case (powAs)
    case (0)
        select case (powLperp)
            case (0)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (1)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (2)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (3)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case default
                Ibar_select = 0._dp
        end select
    case (1)
        select case (powLperp)
            case (0)
                select case (type_in)
                    case (1)
                        Ibar_select = z*(2.6666666666666665_dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (1)
                select case (type_in)
                    case (1)
                        Ibar_select = (-5.333333333333333_dp)/z + z*(-2.6666666666666665_dp) + (5.333333333333334_dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (2)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (3)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case default
                Ibar_select = 0._dp
        end select
    case (2)
        select case (powLperp)
            case (0)
                select case (type_in)
                    case (1)
                        Ibar_select = (z**3*(-86.2200328407225_dp) + z**5*(-72.93175074183976_dp) + (-44.34762556464744_dp) + &
                        z*(-43.76700883311665_dp) + z**7*(-1.2441960447119518_dp) + z**6*(7.84991423670669_dp) + &
                        Nf*(z**3*(-25.001344086021508_dp) + z*(-16.98765432098765_dp) + z**5*(-0.7109533468559838_dp) + &
                        z**6*(-0.06066734074823053_dp) + z**7*(-0.0201765447667087_dp) + z**4*(10.77611140867702_dp) + &
                        (11.061728395061728_dp) + z**2*(23.51085568326948_dp)) + z**2*(104.68828446318862_dp) + &
                        z**4*(124.82050788764909_dp))/z + ((-66.66666666666667_dp) + z*(-39.111111111111114_dp) + &
                        z**2*(2.688618925831202_dp) + z**3*(67.72398685651697_dp))*Log(z) + (z**2*(0.1952662721893491_dp) + &
                        z*(9.333333333333334_dp) + z**3*(11.018819503849445_dp) + (12.444444444444445_dp))*Log(z)**2 + &
                        ((-4.148148148148148_dp) + z*(-3.259259259259259_dp) + z**2*(0.001443001443001443_dp) + &
                        z**3*(2.7276657060518734_dp))*Log(z)**3 + (z*(-174.33771036747885_dp) + z**3*(-101.27627627627628_dp) + &
                        (-4.802616287732057_dp) + Nf*(z*(-28.970198612185694_dp) + z**3*(-3.6768968456947997_dp) + &
                        (16.202206438801_dp) + z**2*(17.630074204264677_dp)) + z**2*(259.9721584870427_dp))*Log(z*(-1._dp) + &
                        (1._dp)) + (z**2*(-106.35646527385659_dp) + (-79.07451533973273_dp) + Nf*(z*(-5.521815574158203_dp) + &
                        z**3*(-0.31933701657458563_dp) + z**2*(2.7954688674965356_dp) + (3.934572612125142_dp)) + &
                        z**3*(26.974308300395258_dp) + z*(153.56778342430516_dp))*Log(z*(-1._dp) + (1._dp))**2 + &
                        (z*(-19.089851828036377_dp) + z**3*(-8.67537122375832_dp) + (2.985018079916806_dp) + &
                        z**2*(23.298723490396412_dp))*Log(z*(-1._dp) + (1._dp))**3

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (1)
                select case (type_in)
                    case (1)
                        Ibar_select = (z**2*(-334.49068211099967_dp) + z**4*(-178.48953068592058_dp) + z*(-16.03917053805621_dp) + &
                        z**5*(-7.902693310165074_dp) + z**7*(0.21675531914893617_dp) + z**6*(2.719227674979887_dp) + &
                        (10.318945069571619_dp) + Nf*(z**3*(-24.312534209085932_dp) + z*(-18.962962962962962_dp) + &
                        z**5*(-0.8577745025792188_dp) + z**7*(-0.08584686774941995_dp) + z**6*(-0.030690537084398978_dp) + &
                        z**4*(10.164082687338501_dp) + (11.851851851851851_dp) + z**2*(29.937580437580444_dp)) + &
                        z**3*(430.70112079701113_dp))/z + (z**2*(0.3026389668725435_dp) + z**3*(10.917067307692308_dp) + &
                        (49.77777777777778_dp) + z*(62.22222222222222_dp))*Log(z) + ((-24.88888888888889_dp) + &
                        z*(-19.555555555555557_dp) + z**2*(0.03954802259887006_dp) + z**3*(1.697350069735007_dp))*Log(z)**2 + &
                        (z**2*(-395.10632248462554_dp) + (-325.42735156739957_dp) + Nf*(z*(-31.278707683663434_dp) + &
                        z**3*(-4.588987217305801_dp) + (18.67819356651215_dp) + z**2*(20.74505689001264_dp)) + &
                        z**3*(88.17281879194631_dp) + z*(563.0275219267455_dp))*Log(z*(-1._dp) + (1._dp)) + ((-28.05023086242759_dp) &
                        + z**3*(-8.163377192982455_dp) + z**2*(6.0543013013151_dp) + z*(21.27041786520606_dp))*Log(z*(-1._dp) + &
                        (1._dp))**2

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (2)
                select case (type_in)
                    case (1)
                        Ibar_select = z**2*(-151.83575418994414_dp) + (-82.66666666666669_dp)/z + z**4*(-9.073199527744983_dp) + &
                        z**6*(-1.1011826544021024_dp) + z**5*(1.3794506612410986_dp) + (24.8888888888889_dp) + &
                        z**3*(70.8823244552058_dp) + z*(152.8594733461785_dp) + (z*(-35.55555555555556_dp) + (-32._dp)/z + &
                        (-24.88888888888889_dp) + z**2*(0.15075707702435814_dp) + z**3*(1.5782652043868395_dp))*Log(z) + &
                        (z*(-209.44223210403788_dp) + z**3*(-31.727268369841344_dp) + (123.52414853376492_dp) + &
                        z**2*(140.7564630512254_dp))*Log(z*(-1._dp) + (1._dp))

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (3)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case default
                Ibar_select = 0._dp
        end select
    case (3)
        select case (powLperp)
            case (0)
                select case (type_in)
                    case (1)
                        Ibar_select = (z**5*(-94511.77090261626_dp) + z*(-7169.740093699399_dp) + z**7*(-342.0538838532933_dp) + &
                        Nf**2*(z**2*(-31.173181882262902_dp) + (-27.279733843564706_dp) + z**3*(-26.247694334650856_dp) + &
                        z**7*(0.02021772939346812_dp) + z**6*(0.1076417419884963_dp) + z**5*(0.8288920056100982_dp) + &
                        z**4*(21.88598781549173_dp) + z*(47.82294346360314_dp)) + z**6*(3406.906761190654_dp) + (9340.6870238563_dp) &
                        + z**2*(20999.973574346088_dp) + z**3*(30566.664811476672_dp) + Nf*(z**5*(-37315.55468746373_dp) + &
                        z**2*(-29146.324885331553_dp) + z**7*(-147.50397072375537_dp) + z*(-65.96810489118803_dp) + &
                        (244.58197514129375_dp) + z**6*(1148.2378807116193_dp) + z**3*(31138.04110829745_dp) + &
                        z**4*(34514.804302991746_dp)) + z**4*(35210.52952516608_dp))/z + (z*(478.2703489846124_dp) + &
                        (2860.5342887466486_dp)/z + (10576.670708024894_dp) + z**2*(20646.511902525468_dp) + &
                        Nf*(z**2*(-36449.28966223132_dp) + (-206.82543760377865_dp) + (66.85499822000071_dp)/z + &
                        z*(333.0650805773116_dp) + z**3*(43827.543301759135_dp)) + z**3*(151301.8124360537_dp))*Log(z) + &
                        ((-1170.7587186262226_dp) + z**2*(595.7540407497181_dp) + (923.1797016265684_dp)/z + &
                        z*(1376.8557494240706_dp) + Nf*(z**2*(-11432.693452380952_dp) + (-104.95370601500125_dp) + &
                        z*(-8.308237388561654_dp) + z**3*(1416.7368821292775_dp)) + z**3*(6192.450254632087_dp))*Log(z)**2 + &
                        (z**2*(-244.7558051372226_dp) + z*(-153.21994034339727_dp) + (911.287682068523_dp) + &
                        Nf*(z**2*(-1575.987044534413_dp) + (-39.46227709190672_dp) + z*(25.29492455418381_dp) + &
                        z**3*(8704.259040590407_dp)) + z**3*(16390.839774993303_dp))*Log(z)**3 + ((-58.76543209876543_dp) + &
                        z**2*(-25.033492822966508_dp) + z*(3.720164609053498_dp) + Nf*(z**2*(-105.47199533255542_dp) + &
                        (-2.831275720164609_dp) + z*(1.1193415637860082_dp) + z**3*(10.557038834951456_dp)) + &
                        z**3*(710.5834378920954_dp))*Log(z)**4 + (z*(-13.948971193415638_dp) + z**2*(-0.7566742944317315_dp) + &
                        (10.587654320987655_dp) + Nf*(z**2*(-2.85130890052356_dp) + (-0.9481481481481482_dp) + &
                        z*(0.4740740740740741_dp) + z**3*(208.85885486018643_dp)) + z**3*(304.8141745894555_dp))*Log(z)**5 + &
                        (z*(-83287.57934917185_dp) + z**3*(-31559.24995948433_dp) + Nf**2*(z*(-62.77291538634297_dp) + &
                        z**3*(-27.913618677042802_dp) + (3.009895268230336_dp) + z**2*(79.38034249885915_dp)) + &
                        (22452.174841868346_dp) + Nf*(z**2*(-64093.50016612267_dp) + (-28875.27347334142_dp) + &
                        z**3*(17191.63440541754_dp) + z*(76164.02824730723_dp)) + z**2*(90454.92088371639_dp))*Log(z*(-1._dp) + &
                        (1._dp)) + ((-6113.997999590717_dp) + z**2*(-4353.898241035487_dp) + Nf**2*(z**2*(-39.295505999708794_dp) + &
                        (-28.17249743907884_dp) + z**3*(10.49977588525325_dp) + z*(52.622548541188706_dp)) + &
                        z**3*(276.44338194242954_dp) + z*(9371.97012076143_dp) + Nf*(z*(-21862.470882213318_dp) + &
                        z**3*(-8352.664173860585_dp) + (6974.898594789018_dp) + z**2*(23363.031642269452_dp)))*Log(z*(-1._dp) + &
                        (1._dp))**2 + (z*(-8481.811550085245_dp) + z**3*(-3621.1800041306424_dp) + Nf**2*((-2.5620097169717466_dp) + &
                        z**3*(-1.2904073587385019_dp) + z*(0.8706491122420283_dp) + z**2*(1.9941136424805657_dp)) + &
                        Nf*(z**2*(-1671.4604442749549_dp) + (-523.4906387395719_dp) + z**3*(535.6529411764706_dp) + &
                        z*(1687.6547947873016_dp)) + (2013.5752176738144_dp) + z**2*(9886.596158383645_dp))*Log(z*(-1._dp) + &
                        (1._dp))**3 + (z**3*(-121.32318710832587_dp) + (-81.98127527815578_dp) + z*(-76.90796165489496_dp) + &
                        z**2*(249.6362923541338_dp) + Nf*(z*(-597.0222306164422_dp) + z**3*(-197.15028901734104_dp) + &
                        (204.2569584538716_dp) + z**2*(592.7962196161257_dp)))*Log(z*(-1._dp) + (1._dp))**4 + &
                        (z*(-255.22204900946286_dp) + z**3*(-89.44044502617801_dp) + (79.18709828793872_dp) + &
                        z**2*(263.006259945233_dp))*Log(z*(-1._dp) + (1._dp))**5

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (1)
                select case (type_in)
                    case (1)
                        Ibar_select = (z**3*(-66939.26451567911_dp) + z**5*(-43640.85076677764_dp) + z**2*(-40820.90029940498_dp) + &
                        (-8329.890560190655_dp) + z**7*(-302.56850852711375_dp) + Nf**2*(z**2*(-77.97445672763347_dp) + &
                        z**4*(-35.920374470295265_dp) + (-12.378600823045268_dp) + z**7*(0.06725397943196539_dp) + &
                        z**6*(0.20222611593983328_dp) + z**5*(2.369844489519946_dp) + z*(32.13168724279836_dp) + &
                        z**3*(83.3378094939442_dp)) + z**6*(2159.5786990167953_dp) + Nf*(z**3*(-5014.506794109375_dp) + &
                        z**5*(-573.9266693274021_dp) + z*(-490.2772938483839_dp) + z**7*(-3.733700223649448_dp) + &
                        z**6*(45.16999768678995_dp) + (518.7498168642495_dp) + z**2*(582.477231789651_dp) + &
                        z**4*(5377.9879395868875_dp)) + z*(11757.52097459584_dp) + z**4*(142844.75300591116_dp))/z + &
                        (z**3*(-47650.75189886759_dp) + z**2*(-44262.56434834571_dp) + (-4425.496954200217_dp) + &
                        (-1721.4369617902225_dp)/z + Nf*(z**3*(-2557.2758045167684_dp) + z**2*(-1953.7330542758182_dp) + &
                        (-198.90742127519263_dp) + (62.84347343625167_dp)/z + z*(124.40761548401011_dp)) + &
                        z*(3168.551435857338_dp))*Log(z) + (z**2*(-6535.2817158676135_dp) + z*(-695.7032048642793_dp) + &
                        Nf*(z**2*(-303.81571620501387_dp) + (-88.09876543209877_dp) + z*(47.30864197530864_dp) + &
                        z**3*(1199.3961589988419_dp)) + (3229.449605856966_dp) + z**3*(37861.97093586552_dp))*Log(z)**2 + &
                        (z**3*(-670.403748053927_dp) + z**2*(-454.6027935456744_dp) + (-286.2222222222223_dp) + &
                        Nf*(z**3*(-83.65199321396027_dp) + z**2*(-21.332259023296757_dp) + (-11.45679012345679_dp) + &
                        z*(3.3580246913580245_dp)) + z*(180.2469135802469_dp))*Log(z)**3 + (z*(-89.23456790123457_dp) + &
                        z**2*(-12.508541392904073_dp) + Nf*((-5.925925925925926_dp) + z**2*(-0.6308139534883721_dp) + &
                        z*(2.962962962962963_dp) + z**3*(29.153427638737757_dp)) + (66.17283950617283_dp) + &
                        z**3*(1014.0617760617761_dp))*Log(z)**4 + (z**2*(-59144.057807899546_dp) + (-37843.51155524647_dp) + &
                        Nf**2*(z**2*(-58.76691672456219_dp) + (-54.007357506349955_dp) + z**3*(12.256322818982666_dp) + &
                        z*(89.45622301686777_dp)) + Nf*(z**2*(-908.5945922619609_dp) + z*(274.65703639520024_dp) + &
                        z**3*(426.1626588310546_dp) + (518.0412908934857_dp)) + z**3*(13525.511008216847_dp) + &
                        z*(80976.31519378009_dp))*Log(z*(-1._dp) + (1._dp)) + (z*(-22516.296518935298_dp) + &
                        z**3*(-10122.749948133172_dp) + Nf**2*((-13.115238157128097_dp) + z**2*(-9.31822567503274_dp) + &
                        z**3*(1.0644567219152854_dp) + z*(18.40604414728259_dp)) + Nf*(z*(-842.8843485514335_dp) + &
                        z**3*(-123.98394647948501_dp) + (490.70946029523355_dp) + z**2*(560.3069828838331_dp)) + &
                        (5134.980188237667_dp) + z**2*(26925.88322640089_dp))*Log(z*(-1._dp) + (1._dp))**2 + &
                        ((-246.6140084175432_dp) + z**3*(-65.88836403461971_dp) + z**2*(-28.91135442237791_dp) + &
                        Nf*(z**2*(-164.32072592121926_dp) + (-48.06436653669776_dp) + z**3*(55.9553401860879_dp) + &
                        z*(158.60259177800194_dp)) + z*(259.33965280046664_dp))*Log(z*(-1._dp) + (1._dp))**3 + &
                        (z*(-946.8394094774993_dp) + z**3*(-313.005082592122_dp) + (321.3616078871331_dp) + &
                        z**2*(941.4458471454511_dp))*Log(z*(-1._dp) + (1._dp))**4

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (2)
                select case (type_in)
                    case (1)
                        Ibar_select = (z**5*(-2828.0740380454235_dp) + z*(-1921.906781696768_dp) + z**2*(-1704.9094560837823_dp) + &
                        z**7*(-59.52085659115983_dp) + Nf**2*(z**2*(-9.386600886600887_dp) + (-3.950617283950617_dp) + &
                        z**4*(-3.3880275624461667_dp) + z**6*(0.010230179028132991_dp) + z**7*(0.028615622583139988_dp) + &
                        z**5*(0.2859248341930729_dp) + z*(6.320987654320989_dp) + z**3*(8.104178371929093_dp)) + &
                        z**6*(214.83343351494656_dp) + Nf*(z*(-229.36726055725282_dp) + z**4*(-178.08205258067804_dp) + &
                        z**2*(-118.18652630559994_dp) + z**6*(-7.277252911524441_dp) + z**7*(4.054541893131286_dp) + &
                        z**5*(114.61486608653492_dp) + z**3*(187.97051798236524_dp) + (308.80249919264116_dp)) + &
                        z**4*(993.2566921070128_dp) + (1738.3991733727978_dp) + z**3*(3011.831480913418_dp))/z + &
                        (z*(18.837203522249037_dp) + (61.91367041742972_dp)/z + Nf*(z**3*(-119.74668719745489_dp) + (-64._dp) + &
                        z**2*(-53.043003643547664_dp) + (111.80246913580247_dp)/z + z*(164.74074074074073_dp)) + &
                        z**2*(358.55947574193874_dp) + z**3*(2470.547336337676_dp) + (3284.4219009299295_dp))*Log(z) + &
                        ((-398.2222222222225_dp) + Nf*(z*(-8.296296296296296_dp) + z**2*(-4.8845898593719355_dp) + &
                        (-4.740740740740743_dp) + z**3*(2.286801370196382_dp)) + z**2*(31.869564066314016_dp) + &
                        z**3*(304.94555354111714_dp) + z*(312.88888888888886_dp))*Log(z)**2 + (z*(-153.28395061728395_dp) + &
                        z**2*(1.9324055666003976_dp) + Nf*((-9.481481481481481_dp) + z**3*(-6.832326283987914_dp) + &
                        z**2*(-0.23712183156173347_dp) + z*(4.7407407407407405_dp)) + (105.87654320987656_dp) + &
                        z**3*(134.59642401021708_dp))*Log(z)**3 + (z**2*(-5382.39168676986_dp) + (-990.4770491448967_dp) + &
                        Nf**2*(z**2*(-6.915018963337549_dp) + (-6.22606452217072_dp) + z**3*(1.5296624057686001_dp) + &
                        z*(10.42623589455448_dp)) + Nf*(z**2*(-160.9406367492202_dp) + (-142.3551668762234_dp) + &
                        z**3*(40.700142343905874_dp) + z*(199.7808464667229_dp)) + z**3*(1543.4946737675941_dp) + &
                        z*(5460.799670155016_dp))*Log(z*(-1._dp) + (1._dp)) + (z*(-5983.700427542028_dp) + &
                        z**3*(-1373.5855449003866_dp) + Nf*(z**2*(-206.6029605030009_dp) + (-162.01908920860333_dp) + &
                        z**3*(50.14378749905067_dp) + z*(290.62641036070187_dp)) + (2939.872256135636_dp) + &
                        z**2*(4901.858160751223_dp))*Log(z*(-1._dp) + (1._dp))**2 + (z*(-384.98661958398407_dp) + &
                        z**3*(-2.737920937042457_dp) + z**2*(158.0816726790372_dp) + (306.6799048790264_dp))*Log(z*(-1._dp) + &
                        (1._dp))**3

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (3)
                select case (type_in)
                    case (1)
                        Ibar_select = ((-1194.651740861711_dp) + z**3*(-548.7415523954633_dp) + z**4*(-365.93375082544543_dp) + &
                        z**6*(-13.901324599354476_dp) + z**7*(17.992705441428722_dp) + Nf*(z**4*(-71.06210158935419_dp) + &
                        z*(-39.50617283950617_dp) + z**2*(-24.010340118847466_dp) + z**6*(-7.181876292953364_dp) + &
                        z**7*(1.6204092319884238_dp) + z**5*(32.278259138366096_dp) + (34.69958847736624_dp) + &
                        z**3*(70.79186246049774_dp)) + z**5*(292.83877689474707_dp) + z**2*(905.1280202572963_dp) + &
                        z*(1112.1290850662403_dp))/z + ((-682.666666666667_dp)/z + (-223.99999999999997_dp) + &
                        z**3*(-47.29348722280993_dp) + z**2*(-5.9592490345760245_dp) + Nf*(z**3*(-3.706019918386917_dp) + &
                        z*(1.5802469135802417_dp) + (3.9506172839506166_dp) + z**2*(7.784015356314111_dp) + &
                        (13.432098765432102_dp)/z) + z*(26.46913580246906_dp))*Log(z) + (z*(-143.80246913580245_dp) + (-64._dp)/z + &
                        z**3*(-5.101605685706764_dp) + z**2*(0.383617193836172_dp) + Nf*(z**3*(-11.12646793134598_dp) + &
                        (-4.7407407407407405_dp) + z**2*(0.7233727810650887_dp) + z*(2.3703703703703702_dp)) + &
                        (52.93827160493826_dp))*Log(z)**2 + (z*(-1677.4861530471046_dp) + z**3*(-130.7999645636753_dp) + &
                        Nf*(z**2*(-45.82480713284435_dp) + (-37.63522883033307_dp) + z**3*(10.8108385339717_dp) + &
                        z*(65.93314804648969_dp)) + z**2*(894.5973443567198_dp) + (984.2072917725787_dp))*Log(z*(-1._dp) + (1._dp)) &
                        + (z**2*(-1021.2985497692816_dp) + (-690.9097544699171_dp) + z**3*(270.28081740276855_dp) + &
                        z*(1336.8410670833434_dp))*Log(z*(-1._dp) + (1._dp))**2

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case default
                Ibar_select = 0._dp
        end select
    case default
        Ibar_select = 0._dp
end select
