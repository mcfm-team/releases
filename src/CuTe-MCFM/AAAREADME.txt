This directory contains a variety of Fortran modules that implement
the resummation extension, CuTe-MCFM

T. Becher and T. Neumann,
``Fiducial $q_T$ resummation of color-singlet processes at N$^3$LL+NNLO,''
JHEP 03, 199 (2021)
[arXiv:2009.11437 [hep-ph]].

T. Neumann,
``The diphoton $q_T$ spectrum at N$^3$LL$^\prime $~+~NNLO,''
Eur. Phys. J. C 81, no.10, 905 (2021)
[arXiv:2107.12478 [hep-ph]].

