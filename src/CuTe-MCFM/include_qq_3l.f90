!
!  SPDX-License-Identifier: GPL-3.0-or-later
!  Copyright (C) 2019-2022, respective authors of MCFM.
!
 
select case (powAs)
    case (0)
        select case (powLperp)
            case (0)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (1._dp)
                        else
                            Ibar_select = (1._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (1)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (2)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (3)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case default
                Ibar_select = 0._dp
        end select
    case (1)
        select case (powLperp)
            case (0)
                select case (type_in)
                    case (1)
                        Ibar_select = z*(-2.6666666666666665_dp) + (2.6666666666666665_dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (-2.1932454224643014_dp)
                        else
                            Ibar_select = (-2.1932454224643014_dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (1)
                select case (type_in)
                    case (1)
                        Ibar_select = (2.6666666666666665_dp) + z*(2.6666666666666665_dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (-4._dp)
                        else
                            Ibar_select = (-4._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (-5.333333333333333_dp)
                        else
                            Ibar_select = (-5.333333333333333_dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (2)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (3)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case default
                Ibar_select = 0._dp
        end select
    case (2)
        select case (powLperp)
            case (0)
                select case (type_in)
                    case (1)
                        Ibar_select = (-19.478221870606756_dp) + z**3*((-52.22838795753341_dp) + Nf*(-4.392485119047619_dp)) + &
                        Nf*(-3.654320928943654_dp) + z**4*((-2.2025221668310504_dp) + Nf*(-0.24639893160354862_dp)) + &
                        z**5*(Nf*(0.017839871037076842_dp) + (0.06960260542677314_dp)) + (2.6451727005890224_dp)/z + &
                        z*(Nf*(-1.8770055143244497_dp) + (8.390702182275046_dp)) + z**2*(Nf*(1.658543476725295_dp) + &
                        (63.36166396971877_dp)) + (z*(-16.88888888888889_dp) + (-5.333333333333334_dp) + &
                        z**2*(-1.857655468080786_dp) + Nf*((1.4814814814814814_dp) + z*(1.4814814814814814_dp) + &
                        z**3*(1.9203081586259156_dp) + z**2*(3.503592902185071_dp)) + z**3*(61.37866450247033_dp))*Log(z) + &
                        (z**3*(-18.786707737231765_dp) + z**2*(-12.803702472061493_dp) + z*(-10.666666666666666_dp) + &
                        (-2.6666666666666665_dp) + Nf*(z**3*(-0.053204192649595326_dp) + (0.4444444444444444_dp) + &
                        z*(0.4444444444444444_dp) + z**2*(0.9395466949666464_dp)))*Log(z)**2 + (z**2*(-2.496058052317927_dp) + &
                        (-0.2962962962962963_dp) + z*(-0.2962962962962963_dp) + z**3*(3.4789361919552912_dp))*Log(z)**3 + &
                        (z*(-18.739650524376295_dp) + z**3*(-3.1972624798711755_dp) + (21.104527355585894_dp) + &
                        z**2*(23.0546078708838_dp))*Log(z*(-1._dp) + (1._dp)) + ((-7.259164154264592_dp) + &
                        z**2*(-6.356310832246821_dp) + z**3*(1.3263511167688924_dp) + z*(5.178012758631411_dp))*Log(z*(-1._dp) + &
                        (1._dp))**2

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (-30.608535416484305_dp) + Nf*(3.4489756184793317_dp)
                        else
                            Ibar_select = (-30.608535416484305_dp) + Nf*(3.4489756184793317_dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = Nf*(5.530864197530864_dp) + (14.926669450170854_dp)
                        else
                            Ibar_select = Nf*(5.530864197530864_dp) + (14.926669450170854_dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (1)
                select case (type_in)
                    case (1)
                        Ibar_select = (-7.703703703703703_dp)/z + Nf*(-1.1851842266201185_dp) + z**2*((-15.681439494386987_dp) + &
                        Nf*(-0.46283902782669945_dp)) + (-0.1676001386040955_dp) + z**5*((-0.12440984013915349_dp) + &
                        Nf*(0.07365718799368089_dp)) + z**4*(Nf*(-0.6510300044782803_dp) + (3.674698435447271_dp)) + &
                        z*(Nf*(-4.742561980915629_dp) + (22.721853107547204_dp)) + z**3*(Nf*(-2.5135230066736916_dp) + &
                        (89.38984667302472_dp)) + (z**2*(-75.21947494222778_dp) + z**3*(-43.591358772461135_dp) + &
                        z*(-39.111111111111114_dp) + (-19.555555555555557_dp) + Nf*((1.7777777777777775_dp) + &
                        z*(1.7777777777777775_dp) + z**3*(1.9345473739773027_dp) + z**2*(3.436207859936674_dp)))*Log(z) + &
                        (z**2*(-16.857399697199092_dp) + (-1.7777777777777781_dp) + z*(-1.7777777777777781_dp) + &
                        z**3*(3.654691276642496_dp))*Log(z)**2 + ((-41.11719493959703_dp) + z**2*(-19.226359593565867_dp) + &
                        z**3*(3.2768045492066404_dp) + z*(28.622305539511814_dp))*Log(z*(-1._dp) + (1._dp))

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (-84.35261963606607_dp) + Nf*(7.75526251932545_dp)
                        else
                            Ibar_select = (-84.35261963606607_dp) + Nf*(7.75526251932545_dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (-54.77591205215827_dp) + Nf*(5.9259259259259265_dp)
                        else
                            Ibar_select = (-54.77591205215827_dp) + Nf*(5.9259259259259265_dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (2)
                select case (type_in)
                    case (1)
                        Ibar_select = z**2*(-2.2092456459304928_dp) + (-1.7777794164880518_dp) + Nf*((-0.8888888888888888_dp) + &
                        z*(-0.8888888888888888_dp)) + z**5*(-0.5918134963442406_dp) + (1.7777777777777777_dp)/z + &
                        z**4*(3.431416450624558_dp) + z*(10.536329359013553_dp) + z**3*(11.055536391859262_dp) + &
                        (z**2*(-13.889402138049359_dp) + z**3*(-8.607642691980042_dp) + (-0.8888888888888888_dp) + &
                        z*(-0.8888888888888888_dp))*Log(z) + (z*(-16.399098675437834_dp) + (-13.467620810972969_dp) + &
                        z**3*(-0.667673630717109_dp) + z**2*(2.0899486726834704_dp))*Log(z*(-1._dp) + (1._dp))

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (-37.39461783961921_dp) + Nf*(1.333333333333333_dp)
                        else
                            Ibar_select = (-37.39461783961921_dp) + Nf*(1.333333333333333_dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (-8._dp) + Nf*(1.7777777777777777_dp)
                        else
                            Ibar_select = (-8._dp) + Nf*(1.7777777777777777_dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (28.444444444444443_dp)
                        else
                            Ibar_select = (28.444444444444443_dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (3)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = 0
                        else
                            Ibar_select = 0
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case default
                Ibar_select = 0._dp
        end select
    case (3)
        select case (powLperp)
            case (0)
                select case (type_in)
                    case (1)
                        Ibar_select = z**3*(-3.3427217363336796e8_dp) + z**4*(-3.2431051381686223e6_dp) + (-1080.5982614131724_dp) + &
                        (-472.185801152152_dp)/z + Nf**2*(z**3*(-13.450264046402909_dp) + z**4*(-0.40541711809317443_dp) + &
                        z**5*(-0.00043641441913240813_dp) + (1.6495029510255073_dp) + z*(7.444117645315636_dp) + &
                        z**2*(20.703478227151113_dp)) + Nf*(z**3*(-2275.457084325846_dp) + z*(-219.92491205988716_dp) + &
                        z**5*(-1.27764543675966_dp) + (5.88282387997262_dp)/z + z**4*(82.20918803777823_dp) + (84.97942723038443_dp) &
                        + z**2*(1984.0321035087022_dp)) + z*(6263.120863556335_dp) + z**5*(39341.60322033864_dp) + &
                        z**2*(3.3747238190769726e8_dp) + ((-1429.24047488185_dp) + (-78.98467140066984_dp)/z + &
                        Nf**2*(-5.662551440329218_dp) + Nf*(153.53624971840847_dp) + z*(Nf**2*(-1.1851851851851851_dp) + &
                        Nf*(28.742781690295573_dp) + (164.6569218640053_dp)) + z**2*(Nf**2*(0.6223743940909441_dp) + &
                        Nf*(979.951826373415_dp) + (1.3738623388470685e8_dp)) + z**3*(Nf**2*(13.50872115760657_dp) + &
                        Nf*(1168.676727125948_dp) + (2.032582522664056e8_dp)))*Log(z) + (z**3*(-5.370075316682352e7_dp) + &
                        (-196.22980905904754_dp) + Nf**2*(z**3*(-4.080640889830509_dp) + (-2.4362139917695473_dp) + &
                        z**2*(-2.3073629620695892_dp) + z*(-0.8559670781893004_dp)) + z*(26.241691401506486_dp) + &
                        Nf*(z**3*(-402.55244147369024_dp) + z*(6.6863345960316956_dp) + (47.65684715008643_dp) + &
                        z**2*(210.75077176993884_dp)) + z**2*(2.3897672173141126e7_dp))*Log(z)**2 + ((-115.53991840076135_dp) + &
                        z*(-47.96790193985601_dp) + Nf**2*(z**2*(-0.6104142911440517_dp) + (-0.3292181069958848_dp) + &
                        z*(-0.3292181069958848_dp) + z**3*(0.6660089450144698_dp)) + Nf*((10.326474622770919_dp) + &
                        z*(11.050754458161865_dp) + z**2*(38.63021707930867_dp) + z**3*(56.80190606582247_dp)) + &
                        z**2*(2.2248225933966124e6_dp) + z**3*(9.716442709638055e6_dp))*Log(z)**3 + (z**3*(-887702.7570951866_dp) + &
                        z*(-10.691358024691358_dp) + (-1.530864197530864_dp) + Nf*(z**3*(-10.936522351273572_dp) + &
                        (0.6090534979423868_dp) + z*(0.6090534979423868_dp) + z**2*(3.378615094127529_dp)) + &
                        z**2*(111106.8459163747_dp))*Log(z)**4 + ((-1.0419753086419754_dp) + z*(0.9728395061728395_dp) + &
                        z**2*(2391.7673040141467_dp) + z**3*(85158.56322382427_dp))*Log(z)**5 + (z*(-8229.839494390406_dp) + &
                        Nf*(z**2*(-30.59983085491485_dp) + z*(-0.11155794282490561_dp) + z**3*(5.808624467512806_dp) + &
                        (14.456967794684449_dp)) + z**3*(102.93734727964954_dp) + z**2*(4165.354484787279_dp) + &
                        (4550.651748188859_dp))*Log(z*(-1._dp) + (1._dp)) + (z**2*(-13798.89473059282_dp) + (-4342.16176940399_dp) + &
                        Nf*(z*(-9.224322621215682_dp) + z**3*(-1.7788024634202817_dp) + (10.043747733218687_dp) + &
                        z**2*(11.329747721787648_dp)) + z**3*(4719.296429028051_dp) + z*(13432.266111454075_dp))*Log(z*(-1._dp) + &
                        (1._dp))**2 + ((-48.032152469655344_dp) + z**3*(-3.6747150447818466_dp) + z**2*(1.4407344100318031_dp) + &
                        Nf*(z*(-0.1234260607052864_dp) + z**3*(0.03754220270917316_dp) + z**2*(0.5815314135316819_dp) + &
                        (1.611348329238094_dp)) + z*(15.500701005639957_dp))*Log(z*(-1._dp) + (1._dp))**3 + &
                        (z**2*(-7.504455857694885_dp) + (-2.2065175424656394_dp) + z**3*(2.525512367491166_dp) + &
                        z*(7.185461032669358_dp))*Log(z*(-1._dp) + (1._dp))**4

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (-2288.313108178154_dp) + Nf**2*(-8.57238037207872_dp) + Nf*(306.7976732366431_dp)
                        else
                            Ibar_select = (-2288.313108178154_dp) + Nf**2*(-8.57238037207872_dp) + Nf*(306.7976732366431_dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = Nf**2*(-9.09324461452157_dp) + (107.39816902904101_dp) + Nf*(142.12668974589386_dp)
                        else
                            Ibar_select = Nf**2*(-9.09324461452157_dp) + (107.39816902904101_dp) + Nf*(142.12668974589386_dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (1)
                select case (type_in)
                    case (1)
                        Ibar_select = z**3*(-4.22972933300531e6_dp) + z**5*(-3089.5349177331004_dp) + (-1757.6987405291425_dp) + &
                        Nf**2*(z**2*(-4.422782608695652_dp) + (-0.6584363525049426_dp) + z**5*(-0.04757297210127399_dp) + &
                        z**4*(0.657063817609463_dp) + z*(7.244031151067413_dp) + z**3*(11.713293621097375_dp)) + &
                        Nf*(z**2*(-436.09391015522965_dp) + z*(-237.36245015658233_dp) + (-1.1565265637483262_dp)/z + &
                        z**5*(-0.4200837414838903_dp) + z**4*(25.05741279537468_dp) + (58.80384996366821_dp) + &
                        z**3*(144.25386156284176_dp)) + (483.9673467888446_dp)/z + z*(2185.746709484631_dp) + &
                        z**4*(135352.07357320288_dp) + z**2*(4.098196640506589e6_dp) + ((-801.1344497497917_dp) + &
                        Nf**2*(-6.320987654320988_dp) + z*((-17.561133205221694_dp) + Nf**2*(-1.580246913580247_dp) + &
                        Nf*(9.34091182062968_dp)) + (98.11244277743864_dp)/z + Nf*(147.6125167589013_dp) + &
                        z**2*(Nf**2*(-9.342914380280464_dp) + Nf*(116.84643790984825_dp) + (1.6390928823203878e6_dp)) + &
                        z**3*(Nf*(-284.68715341647203_dp) + Nf**2*(-5.120821746779411_dp) + (2.3298399152559782e6_dp)))*Log(z) + &
                        (z**3*(-737901.8415141026_dp) + (-497.1782563419991_dp) + z*(-89.92539186663392_dp) + &
                        Nf**2*(z**2*(-2.5054578258908435_dp) + (-1.1851851851851851_dp) + z*(-1.1851851851851851_dp) + &
                        z**3*(0.14187787498872553_dp)) + Nf*(z*(33.77777777777778_dp) + (39.7037037037037_dp) + &
                        z**3*(93.75245473222748_dp) + z**2*(95.34625293470691_dp)) + z**2*(265879.8115445273_dp))*Log(z)**2 + &
                        (z*(-66.37037037037037_dp) + (-11.654320987654323_dp) + Nf*(z**3*(-21.23221015364744_dp) + &
                        (2.9629629629629632_dp) + z*(2.9629629629629632_dp) + z**2*(14.542821676253126_dp)) + &
                        z**2*(20689.513723999866_dp) + z**3*(90225.0765892128_dp))*Log(z)**3 + (z**3*(-14263.124546787465_dp) + &
                        (-6.444444444444445_dp) + z*(6.296296296296296_dp) + z**2*(660.4845220177932_dp))*Log(z)**4 + &
                        ((-416.0510248494995_dp) + z**3*(-388.0581448811905_dp) + z*(-293.1852857091508_dp) + &
                        Nf*(z*(-12.011371315711287_dp) + z**3*(-3.8729492332335544_dp) + z**2*(42.8603769974179_dp) + &
                        (81.40254437457219_dp)) + z**2*(802.4939709767361_dp))*Log(z*(-1._dp) + (1._dp)) + ((-283.3406638471292_dp) &
                        + z**2*(-187.35895209869057_dp) + Nf*(z*(-0.05433895247430164_dp) + z**3*(0.2376982680130455_dp) + &
                        z**2*(2.4854368890323975_dp) + (6.812685276910341_dp)) + z**3*(19.63877375541033_dp) + &
                        z*(204.54232367189087_dp))*Log(z*(-1._dp) + (1._dp))**2 + (z*(-61.54102392563138_dp) + &
                        z**3*(-17.881838821881402_dp) + (51.01477773706017_dp) + z**2*(66.33401093637853_dp))*Log(z*(-1._dp) + &
                        (1._dp))**3

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (-1788.0038383692386_dp) + Nf**2*(-5.729300528155038_dp) + Nf*(339.3314757224809_dp)
                        else
                            Ibar_select = (-1788.0038383692386_dp) + Nf**2*(-5.729300528155038_dp) + Nf*(339.3314757224809_dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (-597.180611891856_dp) + Nf**2*(-6.584362139917696_dp) + Nf*(231.44921352134406_dp)
                        else
                            Ibar_select = (-597.180611891856_dp) + Nf**2*(-6.584362139917696_dp) + Nf*(231.44921352134406_dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (-159.21780746848913_dp) + Nf*(-58.99588477366255_dp)
                        else
                            Ibar_select = (-159.21780746848913_dp) + Nf*(-58.99588477366255_dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (2)
                select case (type_in)
                    case (1)
                        Ibar_select = z**2*(-858.5631726043488_dp) + (-316.407615223112_dp) + (-220.95532436198914_dp)/z + &
                        z**5*(-16.93670096591297_dp) + Nf**2*(z**5*(-0.04910479199578725_dp) + z**2*(0.30855935188446637_dp) + &
                        z**4*(0.43402001177163035_dp) + (0.7901228150561073_dp) + z**3*(1.6756820044491276_dp) + &
                        z*(3.1617079872770857_dp)) + Nf*(z**3*(-162.59090205685936_dp) + z*(-55.49584817664684_dp) + &
                        z**4*(-0.31576611913438773_dp) + z**5*(-0.07353941357773612_dp) + (1.1851851851851851_dp)/z + &
                        (11.315211727782213_dp) + z**2*(50.27870465679118_dp)) + z**4*(124.86727654204465_dp) + &
                        z*(346.28439705409136_dp) + z**3*(1499.9296187480459_dp) + ((-476.40998289081455_dp) + &
                        (-46.22222222222222_dp)/z + Nf**2*(-1.1851851851851851_dp) + z*((-247.28235076067622_dp) + &
                        Nf**2*(-1.1851851851851851_dp) + Nf*(31.01234567901234_dp)) + Nf*(35.75308641975309_dp) + &
                        z**3*((-603.2013327681985_dp) + Nf**2*(-1.2896982493182019_dp) + Nf*(75.03343321959325_dp)) + &
                        z**2*((-1056.7021973040094_dp) + Nf**2*(-2.2908052449521357_dp) + Nf*(126.1268812243271_dp)))*Log(z) + &
                        (z**2*(-418.9234294120876_dp) + z*(-93.92592592592591_dp) + z**3*(-35.0728346050264_dp) + (-32._dp) + &
                        Nf*(z**3*(-10.969063474679714_dp) + (3.5555555555555554_dp) + z*(3.5555555555555554_dp) + &
                        z**2*(21.353051658168518_dp)))*Log(z)**2 + (z**2*(-32.233904473757036_dp) + (-9.876543209876543_dp) + &
                        z**3*(-1.5127359104039044_dp) + z*(11.45679012345679_dp))*Log(z)**3 + ((-412.82558000246803_dp) + &
                        z*(-400.5102010164616_dp) + z**2*(-239.38018717024332_dp) + z**3*(7.8317031716114105_dp) + &
                        Nf*(z**3*(-3.2004963392531387_dp) + z**2*(22.1593548666296_dp) + z*(22.36919903758664_dp) + &
                        (59.8077449041727_dp)))*Log(z*(-1._dp) + (1._dp)) + (z*(-140.08680428408184_dp) + &
                        z**3*(-25.516642384848144_dp) + z**2*(127.28132868227607_dp) + (190.02582169035756_dp))*Log(z*(-1._dp) + &
                        (1._dp))**2

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (-1357.031985332822_dp) + Nf**2*(-5.170175012883632_dp) + Nf*(190.76761759953948_dp)
                        else
                            Ibar_select = (-1357.031985332822_dp) + Nf**2*(-5.170175012883632_dp) + Nf*(190.76761759953948_dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (-223.096709685803_dp) + Nf**2*(-3.950617283950617_dp) + Nf*(74.31423683089888_dp)
                        else
                            Ibar_select = (-223.096709685803_dp) + Nf**2*(-3.950617283950617_dp) + Nf*(74.31423683089888_dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = Nf*(-63.20987654320987_dp) + (646.6620427953394_dp)
                        else
                            Ibar_select = Nf*(-63.20987654320987_dp) + (646.6620427953394_dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (3)
                select case (type_in)
                    case (1)
                        Ibar_select = (-201.3875074710237_dp) + z**2*(-38.23462815087049_dp) + z**4*(-22.37170466331597_dp) + &
                        z*(-12.51025086819331_dp) + Nf**2*((0.3950617283950617_dp) + z*(0.3950617283950617_dp)) + &
                        Nf*(z*(-10.579775121412132_dp) + z**3*(-7.370357594572841_dp) + z**4*(-2.2876109733663923_dp) + &
                        (-1.777776679831051_dp) + (-0.7901234567901234_dp)/z + z**5*(0.3945423271814421_dp) + &
                        z**2*(1.0777687022252667_dp)) + z**5*(2.6164131874537233_dp) + (39.111111111111114_dp)/z + &
                        z**3*(230.6719409454128_dp) + (z**2*(-179.05897772016493_dp) + z**3*(-105.1648184234266_dp) + &
                        z*(-24.8888888888889_dp) + (-14.814814814814824_dp) + (7.111111111111111_dp)/z + Nf*((1.1851851851851851_dp) &
                        + z*(1.1851851851851851_dp) + z**3*(5.738428469070934_dp) + z**2*(9.25960142536624_dp)))*Log(z) + &
                        (z**2*(-13.947536596824612_dp) + (-4.54320987654321_dp) + z*(11.456790123456791_dp) + &
                        z**3*(30.073287001340642_dp))*Log(z)**2 + ((-114.52294112178362_dp) + z*(-101.8766124927582_dp) + &
                        z**2*(-74.30758127876247_dp) + Nf*(z**2*(-1.3932991005450936_dp) + z**3*(0.445115753811406_dp) + &
                        (8.9784138885592_dp) + z*(10.932732421137453_dp)) + z**3*(15.744171930341277_dp))*Log(z*(-1._dp) + (1._dp)) &
                        + (z**3*(-2.6110727599061163_dp) + z**2*(7.681222264632603_dp) + z*(30.396699676379065_dp) + &
                        (40.38500267074629_dp))*Log(z*(-1._dp) + (1._dp))**2
 
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (-308.5478196346396_dp) + Nf**2*(-0.5925925925925923_dp) + Nf*(29.81863411530169_dp)
                        else
                            Ibar_select = (-308.5478196346396_dp) + Nf**2*(-0.5925925925925923_dp) + Nf*(29.81863411530169_dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = Nf**2*(-0.7901234567901234_dp) + Nf*(11.851851851851851_dp) + (101.66018403352467_dp)
                        else
                            Ibar_select = Nf**2*(-0.7901234567901234_dp) + Nf*(11.851851851851851_dp) + (101.66018403352467_dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = Nf*(-18.962962962962962_dp) + (123.25925925925931_dp)
                        else
                            Ibar_select = Nf*(-18.962962962962962_dp) + (123.25925925925931_dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case default
                Ibar_select = 0._dp
        end select
    case default
        Ibar_select = 0._dp
end select
