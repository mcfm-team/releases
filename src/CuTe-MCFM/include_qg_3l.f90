!
!  SPDX-License-Identifier: GPL-3.0-or-later
!  Copyright (C) 2019-2022, respective authors of MCFM.
!
 
select case (powAs)
    case (0)
        select case (powLperp)
            case (0)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (1)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (2)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (3)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case default
                Ibar_select = 0._dp
        end select
    case (1)
        select case (powLperp)
            case (0)
                select case (type_in)
                    case (1)
                        Ibar_select = z*(z*(-2._dp) + (2._dp))
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (1)
                select case (type_in)
                    case (1)
                        Ibar_select = z**2*(-2._dp) + (-1._dp) + z*(2._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (2)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (3)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case default
                Ibar_select = 0._dp
        end select
    case (2)
        select case (powLperp)
            case (0)
                select case (type_in)
                    case (1)
                        Ibar_select = z*(-116.91531083870387_dp) + z**3*(-103.13731364576645_dp) + z**4*(-16.769336143308745_dp) + &
                        z**5*(4.082696945337621_dp) + (5.9516385763253_dp)/z + (6.51093768146994_dp) + z**2*(204.80522133400248_dp) &
                        + (z**2*(-24.73187968628803_dp) + z*(6.666666666666667_dp) + (11.333333333333334_dp) + &
                        z**3*(21.54247493927264_dp))*Log(z) + (z**2*(-20.465598027127005_dp) + (-1.1666666666666667_dp) + &
                        z*(11.666666666666666_dp) + z**3*(15.293497059840886_dp))*Log(z)**2 + (z**2*(-1.0086275130599969_dp) + &
                        z**3*(-0.5403004572175049_dp) + (0.7777777777777778_dp) + z*(2.4444444444444446_dp))*Log(z)**3 + &
                        (z**2*(-179.51737530784183_dp) + (-66.54875304640971_dp) + z**3*(54.8176444640494_dp) + &
                        z*(189.5818172235355_dp))*Log(z*(-1._dp) + (1._dp)) + (z*(-8.33947549500082_dp) + &
                        z**3*(-2.5205581395348835_dp) + (0.5761253443996348_dp) + z**2*(10.283908290136068_dp))*Log(z*(-1._dp) + &
                        (1._dp))**2 + (z**2*(-7.76637769548793_dp) + (-2.489245468238055_dp) + z**3*(2.916343891402715_dp) + &
                        z*(7.8948348278788245_dp))*Log(z*(-1._dp) + (1._dp))**3

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (1)
                select case (type_in)
                    case (1)
                        Ibar_select = z*(-50.80691404374448_dp) + (-17.333333333333332_dp)/z + z**4*(-10.858290621969802_dp) + &
                        z**3*(-9.145891105850524_dp) + z**5*(1.6911060433295324_dp) + (21.632893830259437_dp) + &
                        z**2*(53.2503853487307_dp) + (z**2*(-72.65790509942902_dp) + (-7.333333333333333_dp) + &
                        z*(17.333333333333332_dp) + z**3*(18.23998653651969_dp))*Log(z) + (z**2*(-5.319453991947364_dp) + &
                        z**3*(1.9444742694991948_dp) + (4.666666666666667_dp) + z*(14.666666666666666_dp))*Log(z)**2 + &
                        (z*(-5.497916892354098_dp) + (-2.352382972419054_dp) + z**2*(2.0529827019653624_dp) + &
                        z**3*(5.797317162807791_dp))*Log(z*(-1._dp) + (1._dp)) + (z*(-4.526119553055392_dp) + &
                        z**3*(0.764066016504126_dp) + (2.645092784779759_dp) + z**2*(4.45029408510484_dp))*Log(z*(-1._dp) + &
                        (1._dp))**2

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (2)
                select case (type_in)
                    case (1)
                        Ibar_select = z**2*(-31.000000000000007_dp) + (2.333333333333332_dp) + (4._dp)/z + z*(26.66666666666667_dp) &
                        + (z**2*(-5.333333333333333_dp) + (4.666666666666667_dp) + z*(26.666666666666668_dp))*Log(z) + &
                        (z*(-17.333333333333332_dp) + (8.666666666666668_dp) + z**2*(17.333333333333332_dp))*Log(z*(-1._dp) + &
                        (1._dp))

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (3)
                select case (type_in)
                    case (1)
                        Ibar_select = (0._dp)
                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case default
                Ibar_select = 0._dp
        end select
    case (3)
        select case (powLperp)
            case (0)
                select case (type_in)
                    case (1)
                        Ibar_select = (z**2*(-694771.5000060453_dp) + z*(-1159.4130990003464_dp) + (-1122.3366645410142_dp) + &
                        Nf*(z**2*(-758.3733709715073_dp) + z**5*(-287.42787276181235_dp) + z**6*(-2.0060722275487377_dp) + &
                        (11.954578641471228_dp) + z*(256.9582959516055_dp) + z**3*(417.1572453922548_dp) + &
                        z**4*(442.25008552856656_dp)) + z**6*(1265.6419248539194_dp) + z**5*(4365.665280900331_dp) + &
                        z**3*(95793.58871805332_dp) + z**4*(594981.9678463419_dp))/z + (z**3*(-768074.2462285938_dp) + &
                        z**2*(-536598.7264135503_dp) + (-2355.8728839622704_dp) + z*(-1769.1986942544231_dp) + &
                        (-177.71551065150712_dp)/z + Nf*(z**2*(-561.2871173243674_dp) + z*(-71.98083276696133_dp) + &
                        (216.89773656629333_dp) + z**3*(453.907736333136_dp)))*Log(z) + (z**2*(-91729.69787691534_dp) + &
                        (-100.67928621642898_dp) + z*(-74.87166810714174_dp) + Nf*(z**2*(-202.06556363074068_dp) + &
                        z**3*(-119.87409778812572_dp) + z*(-75.78325472728147_dp) + (67.23388703965145_dp)) + &
                        z**3*(221190.33563270248_dp))*Log(z)**2 + (z**3*(-34083.821526722284_dp) + z**2*(-8962.498892970549_dp) + &
                        (-172.79383354273608_dp) + Nf*(z*(-21.637860082304528_dp) + z**2*(-20.142619680851062_dp) + &
                        (14.263374485596708_dp) + z**3*(120.4826609640504_dp)) + z*(170.13609352560715_dp))*Log(z)**3 + &
                        (z**2*(-463.9959882901442_dp) + Nf*(z**3*(-4.682166732206056_dp) + z*(-3.654320987654321_dp) + &
                        z**2*(-3.104622898300718_dp) + (1.271604938271605_dp)) + (5.080246913580247_dp) + z*(65.88271604938272_dp) + &
                        z**3*(3952.9617292300754_dp))*Log(z)**4 + (z**3*(-259.0994455049541_dp) + z**2*(-10.131536475145255_dp) + &
                        (-1.9851851851851852_dp) + Nf*(z*(-0.35555555555555557_dp) + z**2*(-0.05652110413319702_dp) + &
                        (0.17777777777777778_dp) + z**3*(3.236072206660442_dp)) + z*(10.264197530864198_dp))*Log(z)**5 + &
                        (z**2*(-1.5566149919806875e6_dp) + (-693546.9978914413_dp) + Nf*(z**2*(-1006.8647775213462_dp) + &
                        (-467.64705600611205_dp) + z**3*(267.94036979969184_dp) + z*(1228.8232996089628_dp)) + &
                        z**3*(431546.870521429_dp) + z*(1.8185152483274792e6_dp))*Log(z*(-1._dp) + (1._dp)) + &
                        (z*(-237028.73863117665_dp) + z**3*(-107803.26885563733_dp) + Nf*(z*(-276.84972079414723_dp) + &
                        z**3*(-116.9679720483331_dp) + (83.34828178031447_dp) + z**2*(311.9507223282516_dp)) + (64530.1134571651_dp) &
                        + z**2*(280267.68273180583_dp))*Log(z*(-1._dp) + (1._dp))**2 + (z**2*(-74362.79530918912_dp) + &
                        (-27325.900826678386_dp) + Nf*(z**2*(-34.8770972460037_dp) + (-13.356512894823108_dp) + &
                        z**3*(9.97839917147507_dp) + z*(37.43216570186203_dp)) + z**3*(23529.678926234235_dp) + &
                        z*(78171.94971539348_dp))*Log(z*(-1._dp) + (1._dp))**3 + (z*(-5013.1927508007975_dp) + &
                        z**3*(-1791.2076153131773_dp) + Nf*(z*(-7.3892448014815235_dp) + z**3*(-2.6122063956230543_dp) + &
                        (2.388519202929235_dp) + z**2*(7.458611006521022_dp)) + (1613.8938023117116_dp) + &
                        z**2*(5192.867674913375_dp))*Log(z*(-1._dp) + (1._dp))**4 + (z**2*(-799.8276852791904_dp) + &
                        (-269.2723548239249_dp) + z**3*(264.81470226466973_dp) + z*(803.3594119125196_dp))*Log(z*(-1._dp) + &
                        (1._dp))**5

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (1)
                select case (type_in)
                    case (1)
                        Ibar_select = (z**3*(-129853.46299731074_dp) + z**5*(-6806.1606047527985_dp) + z*(-2983.878573188233_dp) + &
                        z**6*(857.6941500905778_dp) + (1177.71869631644_dp) + Nf*(z**3*(-2289.180446617168_dp) + &
                        z**2*(-237.23538046745549_dp) + z**5*(-105.80302534215178_dp) + (-0.7456847915865921_dp) + &
                        z**6*(0.8514062046970137_dp) + z*(225.71154970332464_dp) + z**4*(2455.2093695051494_dp)) + &
                        z**2*(68034.69546514687_dp) + z**4*(69266.04261275455_dp))/z + (z*(-406.09413789678945_dp) + &
                        (-142.03277970340008_dp) + Nf*(z**3*(-1318.2484107185967_dp) + z**2*(-789.4531928480204_dp) + &
                        z*(-41.05622138871932_dp) + (170.13855597230702_dp)) + (220.75299624923696_dp)/z + &
                        z**2*(2706.0171943820083_dp) + z**3*(4337.465476956942_dp))*Log(z) + (z**2*(-943.3366050665269_dp) + &
                        (-686.5753907919999_dp) + z*(368.46612175335054_dp) + Nf*(z**2*(-108.8747624649353_dp) + &
                        z*(-44.51851851851852_dp) + (58.03703703703704_dp) + z**3*(447.7548033189073_dp)) + &
                        z**3*(7877.884784996017_dp))*Log(z)**2 + (z**2*(-150.96362083673992_dp) + Nf*(z**3*(-50.083411045487516_dp) &
                        + z*(-16.74074074074074_dp) + z**2*(-14.79422398785877_dp) + (7.037037037037037_dp)) + &
                        (19.814814814814817_dp) + z*(210.96296296296296_dp) + z**3*(748.3480083894103_dp))*Log(z)**3 + &
                        ((-12.407407407407407_dp) + z**2*(-4.964967156709415_dp) + Nf*(z*(-2.2222222222222223_dp) + &
                        z**2*(-0.30381875741986547_dp) + (1.1111111111111112_dp) + z**3*(10.512828074314362_dp)) + &
                        z*(65.44444444444444_dp) + z**3*(245.79940434477922_dp))*Log(z)**4 + (z*(-164498.7812499988_dp) + &
                        z**3*(-36531.95653134886_dp) + Nf*(z**3*(-15.811826411840782_dp) + z*(-5.1282702059057215_dp) + &
                        (7.8804441192546975_dp) + z**2*(11.417166543662496_dp)) + (63642.33315537591_dp) + &
                        z**2*(137401.6990514342_dp))*Log(z*(-1._dp) + (1._dp)) + (z**2*(-41550.78233079086_dp) + &
                        (-11006.106814397284_dp) + Nf*(z**2*(-8.082547674742711_dp) + (-3.7232480914246526_dp) + &
                        z**3*(-0.5610909490817112_dp) + z*(8.663183011545371_dp)) + z**3*(15337.26041997336_dp) + &
                        z*(37323.251826545704_dp))*Log(z*(-1._dp) + (1._dp))**2 + (z*(-4693.423208402095_dp) + &
                        z**3*(-1371.582746152572_dp) + Nf*(z**2*(-1.0949968769720386_dp) + (-0.6014752054707988_dp) + &
                        z**3*(-0.12360953461975029_dp) + z*(1.0793408763218468_dp)) + (1679.2906014951318_dp) + &
                        z**2*(4395.715353059536_dp))*Log(z*(-1._dp) + (1._dp))**3 + (z**2*(-1102.370407606106_dp) + &
                        (-364.2159460270891_dp) + z**3*(364.8179715302491_dp) + z*(1093.2498635844274_dp))*Log(z*(-1._dp) + &
                        (1._dp))**4

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (2)
                select case (type_in)
                    case (1)
                        Ibar_select = (z**2*(-1761.738355258621_dp) + z**4*(-1623.8628770433393_dp) + (-526.482813147809_dp) + &
                        z**5*(-511.5079224819828_dp) + z*(-29.82600718599596_dp) + Nf*(z**2*(-126.39616515865728_dp) + &
                        z**4*(-23.250231248743212_dp) + z**5*(-1.66601235914213_dp) + z**6*(-0.13370149687545416_dp) + &
                        (1.876543209876543_dp) + z*(72.35424987622878_dp) + z**3*(78.19436723004364_dp)) + &
                        z**6*(85.32015506632303_dp) + z**3*(4366.624363057692_dp))/z + (z**2*(-830.997242227052_dp) + &
                        (-704.081670491375_dp) + z*(-250.14255445319202_dp) + (-104._dp)/z + Nf*(z*(-34.51851851851852_dp) + &
                        z**3*(17.158043940467753_dp) + z**2*(38.95244150559512_dp) + (50.370370370370374_dp)) + &
                        z**3*(559.775732516814_dp))*Log(z) + (z**2*(-224.0978772125856_dp) + Nf*(z*(-20.444444444444443_dp) + &
                        z**3*(-3.468035118090763_dp) + z**2*(-2.232350732256853_dp) + (8.88888888888889_dp)) + &
                        (13.333333333333336_dp) + z**3*(324.3509263900062_dp) + z*(352.00000000000006_dp))*Log(z)**2 + &
                        ((-19.851851851851855_dp) + z**2*(-10.81862877519045_dp) + Nf*(z*(-3.555555555555556_dp) + &
                        z**2*(0.06560190073917635_dp) + z**3*(1.2826381569040808_dp) + (1.777777777777778_dp)) + &
                        z**3*(31.844111969111964_dp) + z*(114.96296296296295_dp))*Log(z)**3 + (z**2*(-3162.5688947847793_dp) + &
                        (-947.2665819032279_dp) + Nf*(z**2*(-19.936639321570453_dp) + (-8.676849507603912_dp) + &
                        z**3*(-1.8515865744982094_dp) + z*(20.83544577404294_dp)) + z**3*(1290.873378662998_dp) + &
                        z*(3122.295431358342_dp))*Log(z*(-1._dp) + (1._dp)) + (z*(-255.23219381134624_dp) + &
                        z**3*(-110.5392076214939_dp) + Nf*(z**2*(-2.4704520791863733_dp) + (-1.194809684709898_dp) + &
                        z**3*(0.08226564168268204_dp) + z*(2.471885011102477_dp)) + (80.79093753937059_dp) + &
                        z**2*(296.6471305601362_dp))*Log(z*(-1._dp) + (1._dp))**2 + (z**2*(-248.67192543336725_dp) + &
                        (-94.2694498179906_dp) + z**3*(62.75679336324389_dp) + z*(251.29569299922508_dp))*Log(z*(-1._dp) + &
                        (1._dp))**3

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case (3)
                select case (type_in)
                    case (1)
                        Ibar_select = z**2*(-249.87674040142244_dp) + (-160.4130925141413_dp) + z**3*(-20.612096754936847_dp) + &
                        z**4*(-0.4942348008385712_dp) + z**5*(-0.010576844845821838_dp) + Nf*(z*(-19.851851851851844_dp) + &
                        (-1.6790123456790123_dp)/z + (7.6296296296296315_dp) + z**2*(13.012345679012347_dp)) + &
                        (90.66666666666666_dp)/z + z*(417.5626614919019_dp) + (z**2*(-72.97707069550948_dp) + &
                        z**3*(-39.692601948754955_dp) + Nf*(z*(-6.518518518518518_dp) + (1.9259259259259256_dp)) + &
                        (12.22222222222222_dp) + (16._dp)/z + z*(303.7037037037037_dp))*Log(z) + ((-9.925925925925926_dp) + &
                        z**2*(-4.904683840749415_dp) + Nf*(z*(-1.7777777777777775_dp) + (0.8888888888888887_dp)) + &
                        z**3*(4.875785001847063_dp) + z*(107.85185185185183_dp))*Log(z)**2 + (z*(-233.53429955542347_dp) + &
                        Nf*(z**2*(-5.037037037037037_dp) + (-2.518518518518518_dp) + z*(5.037037037037037_dp)) + &
                        z**3*(53.08120874462546_dp) + (64.49293933520963_dp) + z**2*(142.40459592003285_dp))*Log(z*(-1._dp) + &
                        (1._dp)) + (z**2*(-53.77538749685294_dp) + (-31.221091862450702_dp) + z**3*(-8.426555886502587_dp) + &
                        z*(54.01562783839883_dp))*Log(z*(-1._dp) + (1._dp))**2

                    case (2)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (3)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (4)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case (5)
                        if (z == 1._dp) then
                            Ibar_select = (0._dp)
                        else
                            Ibar_select = (0._dp)
                        endif
                    case default
                        Ibar_select = 0._dp
                end select
            case default
                Ibar_select = 0._dp
        end select
    case default
        Ibar_select = 0._dp
end select
