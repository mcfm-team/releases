
c***********************************************************************
c     Definition to use for computing delta-R and applying jet cuts    *
c         userap=.false.    pseudorapidity                             *
c         userap=.true.     rapidity [as in FASTJET, default]          *
c***********************************************************************
      logical:: userap
      common/userap/userap
