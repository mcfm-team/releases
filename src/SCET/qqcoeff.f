!
!  SPDX-License-Identifier: GPL-3.0-or-later
!  Copyright (C) 2019-2022, respective authors of MCFM.
!
      subroutine qqcoeff(Qsq,musq,coeff)
      implicit none
c   Wilson Coefficients for qq->V in units of
c   as/4/pi
      include 'types.f'
      include 'constants.f'
      include 'scet_const.f'
      real(dp),intent(in)::Qsq,musq
      complex(dp)::L,lnrat,HF,HA,HTF
      complex(dp),intent(out)::coeff(2)

      include 'scet_beta.f'

      L=lnrat(-Qsq,musq)

c     0607228, Eq.51
c     Hard functions are simply related to MSbar subtracted
c     on-shell matrix elements
c     see also 0605068, Eq 24
      coeff(1)=CF*(-(L**2-3*L+8._dp-zeta2))

      HF=0.5_dp*L**4-3*L**3+(12.5_dp-pisq/6)*L**2
     & +(-22.5_dp-3*pisq/2+24*zeta3)*L
     & +255/8._dp+3.5_dp*pisq-83*pi**4/360._dp-30*zeta3
      HA=11/9._dp*L**3+(pisq/3._dp-233/18._dp)*L**2
     & +(2545/54._dp+11/9._dp*pisq-26*zeta3)*L
     & -51157/648._dp-337*pisq/108._dp+11*pi**4/45._dp+313/9._dp*zeta3
      HTF=(-4/9._dp*L**3+38/9._dp*L**2
     & -(418/27._dp+4*pisq/9._dp)*L
     & +4085/162._dp+23*pisq/27._dp+4/9._dp*zeta3)
      coeff(2)=CF*(CF*HF+CA*HA+TF*nflav*HTF)
      return
      end

